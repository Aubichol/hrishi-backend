FROM golang:1.13.1-alpine3.10
RUN mkdir /app
ADD . /app
WORKDIR /app
RUN GO111MODULE=auto go build -o server ./cmd/server/main.go
CMD ["/app/server"]