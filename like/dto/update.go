package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Update provides dto for user like update
type Update struct {
	LikeStr  string `json:"like_str"`
	LikeID   string `json:"like_id"`
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
	Active   bool   `json:"is_active"`
}

//Validate validates like_str update data
func (s *Update) Validate(validate *validator.Validate) error {
	if err := validate.Struct(s); err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			errors.Base{"Invalid like_string update data", false},
		})
	}
	return nil
}

//FromReader decodes like_str update data from request
func (s *Update) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(s)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid like_str update data", false},
		})
	}

	return nil
}
