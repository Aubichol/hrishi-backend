package dto

//CountByStatusReq helps gather the data for reading status by user
type CountByStatusReq struct {
	StatusID string
}
