package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Activate provides dto for user likes active state
type Activate struct {
	LikeID   string `json:"id"`
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
	Active   bool   `json:"is_active"`
}

//Validate validates like data
func (a *Activate) Validate(validate *validator.Validate) error {
	if err := validate.Struct(a); err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			errors.Base{"invalid data", false},
		})
	}
	return nil
}

//FromReader reads data from request body
func (a *Activate) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(a)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid like data", false},
		})
	}

	return nil
}
