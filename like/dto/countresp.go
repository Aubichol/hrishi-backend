package dto

import "strconv"

//CountResp holds the response data for reading like
type CountResp struct {
	Count string `json:"like_count"`
}

//FromModel converts the model data to response data
func (r *CountResp) FromModel(likecnt int64) {
	r.Count = strconv.FormatInt(likecnt, 10)
}
