package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadByStatusResp holds the response data for reading status
type ReadByStatusResp struct {
	Likes []ReadResp `json:"likes"`
}

//FromModel converts the model data of like to response data
func (r *ReadByStatusResp) FromModel(like []*model.Like) {
	var tmp ReadResp
	for _, val := range like {
		tmp.ID = val.ID
		tmp.LikeStr = val.LikeStr
		tmp.Sender = val.UserID
		r.Likes = append(r.Likes, tmp)
	}
}
