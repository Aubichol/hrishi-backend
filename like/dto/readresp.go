package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadResp holds the response data for reading like
type ReadResp struct {
	ID      string `json:"like_id"`
	LikeStr string `json:"like_str"`
	Sender  string `json:"sender"`
}

//FromModel converts the model data to response data
func (r *ReadResp) FromModel(like *model.Like) {
	r.LikeStr = like.LikeStr
	r.Sender = like.UserID
	r.ID = like.ID
}
