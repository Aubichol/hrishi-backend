package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Like provides dto for user likes
type Like struct {
	ID       string `json:"like_id"`
	LikeStr  string `json:"like_str"`
	GiverID  string `json:"giver_id"`
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
	Active   bool   `json:"is_active"`
}

//Validate validates like data
func (l *Like) Validate(validate *validator.Validate) error {
	if err := validate.Struct(l); err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			errors.Base{"invalid data", false},
		})
	}
	return nil
}

func (l *Like) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(l)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid like data", false},
		})
	}

	return nil
}
