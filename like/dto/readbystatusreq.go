package dto

//ReadByStatusReq helps gather the data for reading status by user
type ReadByStatusReq struct {
	StatusID string
}
