package dto

import "fmt"

// BaseResponse provides base response for likes
type BaseResponse struct {
	Message string `json:"message"`
	OK      bool   `json:"ok"`
}

// String provides string repsentation
func (b *BaseResponse) String() string {
	return fmt.Sprintf("message:%s, ok:%v", b.Message, b.OK)
}

// CreateResponse provides base response for likes
type CreateResponse struct {
	Message  string `json:"message"`
	OK       bool   `json:"ok"`
	ID       string `json:"like_id"`
	LikeTime string `json:"like_time"`
	UserID   string `json:"user_id"`
	Active   bool   `json:"is_active"`
}

// String provides string repsentation
func (b *CreateResponse) String() string {
	return fmt.Sprintf("message:%s, ok:%v", b.Message, b.OK)
}

// ActivateResponse provides base response for likes
type ActivateResponse struct {
	Message string `json:"message"`
	OK      bool   `json:"ok"`
	ID      string `json:"like_id"`
	UserID  string `json:"giver_id"`
	Active  bool   `json:"is_active"`
}

// String provides string repsentation
func (a *ActivateResponse) String() string {
	return fmt.Sprintf("message:%s, ok:%v", a.Message, a.OK)
}
