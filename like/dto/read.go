package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
)

//ReadReq stores like related read request
type ReadReq struct {
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
	LikeID   string `json:"like_id"`
}

//FromReader reads comment (?) from request body
func (r *ReadReq) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(r)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"Invalid like data", false},
		})
	}

	return nil
}
