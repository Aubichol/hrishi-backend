package like

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	"gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	"gitlab.com/Aubichol/hrishi-backend/store/like"
	"go.uber.org/dig"
)

//Reader provides an interface for reading likes
type Reader interface {
	Read(*dto.ReadReq) (*dto.ReadResp, error)
}

//likeReader implements Reader interface
type likeReader struct {
	likes   like.Like
	friends friendrequest.FriendRequests
}

//Read implements Reader interface
func (read *likeReader) Read(likeReq *dto.ReadReq) (
	*dto.ReadResp, error,
) {

	like, err := read.likes.FindByID(likeReq.LikeID)

	if err != nil {
		logrus.Error("Could not find like error : ", err)
		return nil, &errors.Unknown{
			errors.Base{"Invalid request", false},
		}
	}

	var resp dto.ReadResp
	resp.FromModel(like)

	giverID := like.UserID

	if giverID == likeReq.UserID {
		return &resp, nil
	}

	uniqueTag := tag.Unique(giverID, likeReq.UserID)

	currentRequest, err := read.friends.FindByUniqueTag(uniqueTag)

	if err != nil {
		logrus.Error("Could not find friendship error : ", err)
		return nil, &errors.Unknown{
			errors.Base{"Invalid request", false},
		}
	}

	if currentRequest.Status != "accepted" {
		return nil, err
	}

	return &resp, nil
}

//NewReaderParams lists params for the NewReader
type NewReaderParams struct {
	dig.In
	Like    like.Like
	Friends friendrequest.FriendRequests
}

//NewRead provides Reader
func NewRead(params NewReaderParams) Reader {
	return &likeReader{
		likes:   params.Like,
		friends: params.Friends,
	}
}
