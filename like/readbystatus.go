package like

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/store/like"
	"go.uber.org/dig"
)

//ByStatusReader provides an interface for reading statuses
type ByStatusReader interface {
	Read(*dto.ReadByStatusReq, int64, int64) (*dto.ReadByStatusResp, error)
}

//likeByUserReader implements Reader interface
type likesByStatusReader struct {
	likes like.Like
}

func (read *likesByStatusReader) Read(likesByStatusReq *dto.ReadByStatusReq, skip int64, limit int64) (*dto.ReadByStatusResp, error) {

	likes, err := read.likes.FindByStatusID(
		likesByStatusReq.StatusID,
		skip, limit,
	)

	if err != nil {
		logrus.Error("Could not find like by status id error : ", err)
		return nil, &errors.Unknown{
			errors.Base{"Invalid request", false},
		}
	}

	var resp dto.ReadByStatusResp
	resp.FromModel(likes)

	return &resp, nil
}

//NewByStatusReaderParams lists params for the NewReader
type NewByStatusReaderParams struct {
	dig.In
	Like like.Like
}

//NewByStatusReader provides Reader
func NewByStatusReader(params NewByStatusReaderParams) ByStatusReader {
	return &likesByStatusReader{
		likes: params.Like,
	}
}
