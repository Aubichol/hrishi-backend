package like

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storelike "gitlab.com/Aubichol/hrishi-backend/store/like"
	"gopkg.in/go-playground/validator.v9"
)

// Creater provides create method for creating like
type Creater interface {
	Create(create *dto.Like) (*dto.CreateResponse, error)
}

// create creates like
type create struct {
	storeLike storelike.Like
	validate  *validator.Validate
}

func (c *create) toModel(userlike *dto.Like) (like *model.Like) {
	like = &model.Like{}
	like.CreatedAt = time.Now().UTC()
	like.UpdatedAt = like.CreatedAt
	like.LikeStr = userlike.LikeStr
	like.UserID = userlike.UserID
	like.StatusID = userlike.StatusID
	return
}

//Create implements Creater interface
func (c *create) Create(create *dto.Like) (
	*dto.CreateResponse, error,
) {
	if err := create.Validate(c.validate); err != nil {
		return nil, err
	}

	modelLike := c.toModel(create)

	ache, err := c.storeLike.CheckExistence(
		modelLike.StatusID, modelLike.UserID,
	)

	if ache {
		errResp := errors.Unknown{
			Base: errors.Base{
				OK:      false,
				Message: "duplicate like",
			},
		}
		err = fmt.Errorf("%s %w", err.Error(), &errResp)
		return nil, err
	}

	id, err := c.storeLike.Save(modelLike)
	if err == nil {
		logrus.WithFields(logrus.Fields{
			"id": modelLike.UserID,
		}).Debug("User created like successfully")

		return &dto.CreateResponse{
			Message:  "like created",
			OK:       true,
			ID:       id,
			UserID:   modelLike.UserID,
			LikeTime: modelLike.CreatedAt.String(),
		}, nil
	}

	logrus.Error("Could not create like ", err)
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return nil, err
}

//NewCreate returns new instance of Creater
func NewCreate(
	storeLikes storelike.Like,
	validate *validator.Validate,
) Creater {
	return &create{
		storeLikes,
		validate,
	}
}
