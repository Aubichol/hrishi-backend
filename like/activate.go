package like

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/model"
	store "gitlab.com/Aubichol/hrishi-backend/store/like"
	"gopkg.in/go-playground/validator.v9"
)

//Activater defines like activater interface
type Activater interface {
	Activate(active *dto.Activate) (*dto.ActivateResponse, error)
}

type activate struct {
	store    store.Like
	validate *validator.Validate
}

func (a *activate) toModel(userlike *dto.Activate) (like *model.Like) {
	like = &model.Like{}
	like.UpdatedAt = time.Now().UTC()
	like.UserID = userlike.UserID
	like.ID = userlike.LikeID
	like.StatusID = userlike.StatusID
	return
}

func (a *activate) Activate(active *dto.Activate) (*dto.ActivateResponse, error) {
	if err := active.Validate(a.validate); err != nil {
		return nil, err
	}

	//transfers request data for passing to store
	modelLike := a.toModel(active)

	//TO-DO: fetching should take place before updating
	_, err := a.store.FindByID(modelLike.ID)

	if err != nil {
		return nil, err
	}
	//send the data to the store
	id, err := a.store.Save(modelLike)

	if err == nil {
		logrus.WithFields(logrus.Fields{
			"id": modelLike.UserID,
		}).Debug("User activated/deactivated like successfully")

		return &dto.ActivateResponse{
			Message: "like activated",
			OK:      true,
			ID:      id,
			UserID:  modelLike.UserID,
			Active:  modelLike.Active,
		}, nil
	}

	//create and serve error response
	logrus.Error("Could not activate like ", err)
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return nil, err
}

//NewActivate provides a controller for like activation
func NewActivate(
	store store.Like,
	validate *validator.Validate,
) Activater {
	return &activate{
		store,
		validate,
	}
}
