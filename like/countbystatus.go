package like

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/store/like"
	"go.uber.org/dig"
)

//CountByStatusReader provides an interface for reading statuses
type CountByStatusReader interface {
	Count(*dto.CountByStatusReq) (*dto.CountResp, error)
}

//statusByUserReader implements Reader interface
type countByStatusReader struct {
	likes like.Like
}

func (read *countByStatusReader) Count(countByStatusReq *dto.CountByStatusReq) (*dto.CountResp, error) {

	likes, err := read.likes.CountByStatusID(countByStatusReq.StatusID)

	if err != nil {
		logrus.Error("Could not find like by status id error : ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	var resp dto.CountResp
	resp.FromModel(likes)

	return &resp, nil
}

//NewCountByStatusReaderParams lists params for the NewReader
type NewCountByStatusReaderParams struct {
	dig.In
	Like like.Like
}

//NewCountByStatusReader provides Reader
func NewCountByStatusReader(params NewByStatusReaderParams) CountByStatusReader {
	return &countByStatusReader{
		likes: params.Like,
	}
}
