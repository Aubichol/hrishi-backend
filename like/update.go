package like

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storelike "gitlab.com/Aubichol/hrishi-backend/store/like"
	"gopkg.in/go-playground/validator.v9"
)

//Updater provides an interface for updating likes
type Updater interface {
	Update(*dto.Update) (*dto.BaseResponse, error)
}

// update updates user like
type update struct {
	storeLike storelike.Like
	validate  *validator.Validate
}

func (u *update) toModel(userlike *dto.Update) (like *model.Like) {
	like = &model.Like{}
	like.UpdatedAt = time.Now().UTC()
	like.LikeStr = userlike.LikeStr
	like.UserID = userlike.UserID
	like.ID = userlike.LikeID
	like.StatusID = userlike.StatusID
	like.Active = userlike.Active
	return
}

//Update implements Update interface
func (u *update) Update(update *dto.Update) (*dto.BaseResponse, error) {
	if err := update.Validate(u.validate); err != nil {
		return nil, err
	}

	modelLike := u.toModel(update)
	_, err := u.storeLike.Save(modelLike)
	if err == nil {
		logrus.WithFields(logrus.Fields{
			"id": modelLike.UserID,
		}).Debug("User updated like successfully")

		return &dto.BaseResponse{
			Message: "like updated",
			OK:      true,
		}, nil
	}

	logrus.Error("Could not update like ", err)
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return nil, err
}

//NewUpdate returns new instance of NewUpdate
func NewUpdate(storeLikes storelike.Like, validate *validator.Validate) Updater {
	return &update{
		storeLikes,
		validate,
	}
}
