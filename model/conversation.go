package model

import (
	"time"
)

// Conversation holds conversation info for a chat a chat group
type Conversation struct {
	ID        string
	UniqueTag string
	UserID    string
	Message   string
	CreatedAt time.Time
}

//Conversations is Conversation array
type Conversations []*Conversation

//Len defines Conversations array length
func (cs Conversations) Len() int {
	return len(cs)
}

//Less compares between two indices and returns whether the first element is smaller
func (cs Conversations) Less(i, j int) bool {
	return cs[i].ID < cs[j].ID
}

func (cs Conversations) Swap(i, j int) {
	cs[i], cs[j] = cs[j], cs[i]
}
