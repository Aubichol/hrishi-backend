package model

import "time"

// Comment defines comment model
type Comment struct {
	ID        string
	UserID    string
	StatusID  string
	Comment   string
	CreatedAt time.Time
	UpdatedAt time.Time
}
