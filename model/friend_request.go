package model

import "time"

// FriendRequest holds data for the friend request
type FriendRequest struct {
	ID         string
	FromUserID string
	ToUserID   string
	BlockerID  string
	UniqueTag  string
	Status     FriendStatus
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
