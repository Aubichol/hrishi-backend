package model

// FriendStatus provides friend's status
type FriendStatus string

//String implements String interface for FriendStatus
func (f FriendStatus) String() string {
	return string(f)
}

// list of friend status
const (
	OpenFriendStatus      FriendStatus = "open"
	PendingFriendStatus   FriendStatus = "pending"
	AcceptedFriendStatus  FriendStatus = "accepted"
	RejectedFriendStatus  FriendStatus = "rejected"
	UnfriendFriendStatus  FriendStatus = "un-friend"
	UnblockedFriendStatus FriendStatus = "unblocked"
	BlockedFriendStatus   FriendStatus = "blocked"
)
