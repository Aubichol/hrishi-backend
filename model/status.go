package model

import "time"

// Status defines status model
type Status struct {
	ID        string
	UserID    string
	Status    string
	CreatedAt time.Time
	UpdatedAt time.Time
}
