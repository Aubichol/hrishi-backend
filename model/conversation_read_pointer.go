package model

// ReadPointer hold conversation read pointer for a user
type ReadPointer struct {
	ID             string
	ConversationID string
	UniqueTag      string
	UserID         string
}
