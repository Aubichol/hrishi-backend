package model

//Picture defines picture model
type Picture struct {
	UserID    string
	PictureID string
}
