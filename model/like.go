package model

import "time"

// Like defines like model
type Like struct {
	ID        string
	UserID    string
	LikeStr   string
	StatusID  string
	Active    bool
	CreatedAt time.Time
	UpdatedAt time.Time
}
