package model

import "time"

// Friend holds friend list
type Friend struct {
	ID         string
	FromUserID string
	ToUserID   string
	Status     FriendStatus
	CreatedAt  time.Time
	UpdatedAt  time.Time
}
