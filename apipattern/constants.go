package apipattern

//LikeCreate holds the api string for creating a like
const LikeCreate string = "/api/v1/like/create"

//LikeUpdate holds the api string for updating a like
const LikeUpdate string = "/api/v1/like/update"

//LikeGet holds the api string for getting a like
const LikeGet string = "/api/v1/like/get"

//GetLikeFromStatus holds the api string for getting like from status id
const GetLikeFromStatus string = "/api/v1/like/getbystatus/{id}"

//CountLikeFromStatus holds the api string for counting likes from status
const CountLikeFromStatus string = "/api/v1/like/countbystatus/{id}"

//LikeActivate holds the api string for activating likes given id
const LikeActivate string = "/api/v1/like/activate"

//CommentCreate holds the api string for creating a comment
const CommentCreate string = "/api/v1/comment/create"

//CountCommentByStatus holds the api string for counting comments from status id
const CountCommentByStatus string = "/api/v1/comment/countbystatus/{id}"

//GetCommentByStatus holds the api string for getting comments by status
const GetCommentByStatus string = "/api/v1/comment/getbystatus/{id}"

//CommentRead holds the api string for reading comments
const CommentRead string = "/api/v1/comment/get/{id}"

//CommentUpdate holds the api string for updating comment
const CommentUpdate string = "/api/v1/comment/update"

//PostChatRoute holds the api string for posting chats
const PostChatRoute string = "/api/v1/friends/{id}/chats"

//ReadChatPointerRoute holds the api string for reading chat pointer route
const ReadChatPointerRoute string = "/api/v1/friends/{id}/chats/pointer"

//ReadChatRoute holds the api string for reading chat route for friends
const ReadChatRoute string = "/api/v1/friends/{id}/chats"

//UpdateChatRoute holds the api string updating chat route
const UpdateChatRoute string = "/api/v1/friends/{id}/chats/update"

//FriendList defines friend list pattern
const FriendList string = "/api/v1/friends"

//PublicUserList holds the api for giving public user list
const PublicUserList string = "/api/v1/public/alluser"

//UserList holds the api for giving user list
const UserList string = "/api/v1/alluser"

//LoginUser holds the api for logging user
const LoginUser string = "/api/v1/users/login"

//RegistrationToken holds holds the api for giving registration token
const RegistrationToken string = "/api/v1/registration/token"

//UserRegistration holds the api for registering a user
const UserRegistration string = "/api/v1/users/registration"

//UserSearch holds the api for searching user
const UserSearch string = "/api/v1/users/search"

//OtherUser holds the api for giving other people's profile information
const OtherUser string = "/api/v1/users/me2/{id}"

//ProfileUpdate holds the api for updating profile information
const ProfileUpdate string = "/api/v1/users/me"

//ProfileUser holds the api for giving information about a user's profile
const ProfileUser string = "/api/v1/users/me"

//StatusCreate holds the api for creating a status by a user
const StatusCreate string = "/api/v1/status/create"

//StatusByUser holds the api for giving status by user id
const StatusByUser string = "/api/v1/status/getbyuser/{id}"

//NewsFeed holds the api for giving news feed given user id
const NewsFeed string = "/api/v1/newsfeed/user/{id}"

//StatusRead holds the api for reading status by id
const StatusRead string = "/api/v1/status/get/{id}"

//StatusUpdate holds the api for updating status
const StatusUpdate string = "/api/v1/status/update"

//PictureDownload holds the api for downloading picture
const PictureDownload string = "/api/v1/pictures/{id}"

//PictureList holds the api for listing pictures
const PictureList string = "/api/v1/pictures/list"

//ProfilePictureSet holds the api for setting profile picture
const ProfilePictureSet string = "/api/v1/profile_pic"

//ProfilePictureUpload holds the api for uploading profile picture
const ProfilePictureUpload string = "/api/v1/profilePics"

//PictureUpload holds the api for uploading a picture
const PictureUpload string = "/api/v1/pictures"

//FriendOpen holds the api for opening friend requests
const FriendOpen string = "/api/v1/friends"

//FriendUpdate holds the api for updating friend requests
const FriendUpdate string = "/api/v1/friends/{id}"
