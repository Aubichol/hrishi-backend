package connection

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
)

type Rejector interface {
	Reject(id, userID string) (*dto.BaseResponse, error)
}

type RejectorFunc func(id, userID string) (*dto.BaseResponse, error)

func (r RejectorFunc) Reject(id, userID string) (*dto.BaseResponse, error) {
	return r(id, userID)
}

func NewRejector(storeFriendRequests storefriendrequest.FriendRequests) Rejector {
	f := func(id, userID string) (*dto.BaseResponse, error) {
		friendReq, err := storeFriendRequests.GetByID(id)
		if err != nil {
			logrus.WithField("err", err).Error("unable to get friendreqeust by id ", id)
			return nil, fmt.Errorf("%s:%w", err.Error(),
				&errors.Unknown{errors.Base{"invalid request", false}},
			)
		}

		if friendReq == nil {
			return nil, &errors.Invalid{errors.Base{"No such requests found", false}}
		}

		if friendReq.ToUserID != userID {
			return nil, &errors.Invalid{errors.Base{"Wrong user tried to reject the friendship status", false}}
		}

		if friendReq.Status != model.PendingFriendStatus {
			return nil, &errors.Invalid{errors.Base{"Unable to reject a status that is not pending", false}}
		}

		if err := storeFriendRequests.Reject(id); err != nil {
			logrus.WithField("err", err).Error("unable to reject friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"invalid request", false}}
		}
		return &dto.BaseResponse{"Rejected", true}, nil
	}

	return RejectorFunc(f)
}
