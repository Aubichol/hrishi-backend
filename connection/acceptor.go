package connection

import (
	"time"

	"github.com/codeginga/locevt"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/event"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	"gitlab.com/Aubichol/hrishi-backend/ws"
)

type Acceptor interface {
	Accept(id, userID string) (*dto.BaseResponse, error)
}

type AcceptorFunc func(id, userID string) (*dto.BaseResponse, error)

func (r AcceptorFunc) Accept(id, userID string) (*dto.BaseResponse, error) {
	return r(id, userID)
}

func fireAcceptEvent(
	usrID string,
	e locevt.Event,
) {
	dto := ws.FriendRequestDTO{
		Event:  "friend_request",
		Action: "new_friend",
	}

	bytes, err := dto.ToBytes()
	if err != nil {
		logrus.Error("tobytes: ", err)
		return
	}

	eventData := event.WSNotificationData{
		UserID: usrID,
		Data:   bytes,
	}

	e.Fire(locevt.FireOption{
		Name:      event.NameWSNotification,
		Data:      &eventData,
		RetryWait: time.Second * 2,
		MaxRetry:  1,
	})
}

func NewAcceptor(
	storeFriendRequests storefriendrequest.FriendRequests,
	event locevt.Event,
) Acceptor {
	f := func(id, userID string) (*dto.BaseResponse, error) {
		friendReq, err := storeFriendRequests.GetByID(id)
		if err != nil {
			logrus.WithField("err", err).Error("unable to get friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"invalid request", false}}
		}

		if friendReq == nil {
			return nil, &errors.Invalid{errors.Base{"You have no connection", false}}
		}

		if friendReq.ToUserID != userID {
			return nil, &errors.Invalid{errors.Base{"wrong way to change this connection", false}}
		}

		if friendReq.Status != model.PendingFriendStatus {
			return nil, &errors.Invalid{errors.Base{"wrong way to change this connection", false}}
		}

		if err := storeFriendRequests.Accept(id); err != nil {
			logrus.WithField("err", err).Error("unable to accept friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"invalid request", false}}
		}

		fireAcceptEvent(friendReq.FromUserID, event)
		fireAcceptEvent(friendReq.ToUserID, event)

		return &dto.BaseResponse{"Accepted", true}, nil
	}

	return AcceptorFunc(f)
}
