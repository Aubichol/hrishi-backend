package connection

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"go.uber.org/dig"
)

//activeFriendList stores database related operations for fetching active friend list
type activeFriendList struct {
	storeFriendRequests storefriendrequest.FriendRequests
	storeUsers          storeuser.Users
}

//Friends implements interface
func (a *activeFriendList) Friends(userID string, skip, limit int) ([]*dto.Friend, error) {
	friendReqs, err := a.storeFriendRequests.ActiveFriends(userID, int64(skip), int64(limit))
	if err != nil {
		logrus.Error("could not get pending list err : ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	ids := []string{}
	mapUserAndReqID := map[string]string{}
	for _, friendReq := range friendReqs {
		if friendReq.ToUserID == userID {
			ids = append(ids, friendReq.FromUserID)
			mapUserAndReqID[friendReq.FromUserID] = friendReq.ID
			continue
		}

		ids = append(ids, friendReq.ToUserID)
		mapUserAndReqID[friendReq.ToUserID] = friendReq.ID
	}

	users, err := a.storeUsers.FindByIDs(ids...)
	if err != nil {
		logrus.Error("could not get users by ids err: ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	friends := []*dto.Friend{}
	for _, user := range users {
		friend := dto.Friend{}
		friend.FromModelUser(user)
		friend.RequestID = mapUserAndReqID[user.ID]
		friends = append(friends, &friend)
	}

	return friends, nil
}

//ActiveFriendListParams lists all the parameters for NewActiveFriendList
type ActiveFriendListParams struct {
	dig.In
	StoreFriendRequests storefriendrequest.FriendRequests
	StoreUsers          storeuser.Users
}

//NewActiveFriendList provides active friendlist
func NewActiveFriendList(params ActiveFriendListParams) FriendList {
	return &activeFriendList{
		storeFriendRequests: params.StoreFriendRequests,
		storeUsers:          params.StoreUsers,
	}
}
