package connection

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
)

type PendingFriendList interface {
	PendingFriends(userID string, skip, limit int) ([]*dto.Friend, error)
}

type pendingFriendList struct {
	storeFriendRequests storefriendrequest.FriendRequests
	storeUsers          storeuser.Users
}

func (p *pendingFriendList) Friends(userID string, skip, limit int) ([]*dto.Friend, error) {
	friendReqs, err := p.storeFriendRequests.PendingRequests(userID, int64(skip), int64(limit))
	if err != nil {
		logrus.Error("could not get pending list err : ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	ids := []string{}
	mapUserAndReqID := map[string]string{}
	for _, friendReq := range friendReqs {
		ids = append(ids, friendReq.FromUserID)
		mapUserAndReqID[friendReq.FromUserID] = friendReq.ID
	}

	users, err := p.storeUsers.FindByIDs(ids...)
	if err != nil {
		logrus.Error("could not get users by ids err: ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	friends := []*dto.Friend{}
	for _, user := range users {
		friend := dto.Friend{}
		friend.FromModelUser(user)
		friend.RequestID = mapUserAndReqID[user.ID]
		friends = append(friends, &friend)

	}

	return friends, nil
}

func NewPendingFriendList(
	storeFriendRequests storefriendrequest.FriendRequests,
	storeUsers storeuser.Users,
) FriendList {
	return &pendingFriendList{
		storeFriendRequests: storeFriendRequests,
		storeUsers:          storeUsers,
	}
}
