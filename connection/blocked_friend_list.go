package connection

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"go.uber.org/dig"
)

//blockedFriendList holds blocked friend list database operations
type blockedFriendList struct {
	storeFriendRequests storefriendrequest.FriendRequests
	storeUsers          storeuser.Users
}

//Friends implements interface
func (a *blockedFriendList) Friends(userID string, skip, limit int) ([]*dto.Friend, error) {
	friendReqs, err := a.storeFriendRequests.BlockedFriends(userID, int64(skip), int64(limit))
	if err != nil {
		logrus.Error("could not get blocked list err : ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	ids := []string{}
	mapUserAndReqID := map[string]string{}
	for _, friendReq := range friendReqs {
		if friendReq.ToUserID == userID {
			ids = append(ids, friendReq.FromUserID)
			mapUserAndReqID[friendReq.FromUserID] = friendReq.ID
			continue
		}

		ids = append(ids, friendReq.ToUserID)
		mapUserAndReqID[friendReq.ToUserID] = friendReq.ID
	}

	users, err := a.storeUsers.FindByIDs(ids...)
	if err != nil {
		logrus.Error("could not get users by ids err: ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	friends := []*dto.Friend{}
	for _, user := range users {
		friend := dto.Friend{}
		friend.FromModelUser(user)
		friend.RequestID = mapUserAndReqID[user.ID]
		friends = append(friends, &friend)
	}

	return friends, nil
}

//blockedFriendListParams lists parameters for NewBlockedFriendList
type blockedFriendListParams struct {
	dig.In
	StoreFriendRequests storefriendrequest.FriendRequests
	StoreUsers          storeuser.Users
}

//NewBlockedFriendList provides blocked friend list
func NewBlockedFriendList(params blockedFriendListParams) FriendList {
	return &blockedFriendList{
		storeFriendRequests: params.StoreFriendRequests,
		storeUsers:          params.StoreUsers,
	}
}
