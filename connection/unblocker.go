package connection

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
)

type Unblocker interface {
	Unblock(id, userID string) (*dto.BaseResponse, error)
}

type UnblockerFunc func(id, userID string) (*dto.BaseResponse, error)

func (r UnblockerFunc) Unblock(id, userID string) (*dto.BaseResponse, error) {
	return r(id, userID)
}

func NewUnblocker(storeFriendRequests storefriendrequest.FriendRequests) Unblocker {
	f := func(id, userID string) (*dto.BaseResponse, error) {
		friendReq, err := storeFriendRequests.GetByID(id)

		if err != nil {
			logrus.WithField("err", err).Error("unable to get friendreqeust by id ", id)
			return nil, fmt.Errorf("%s:%w", err.Error(),
				&errors.Unknown{errors.Base{"invalid request", false}},
			)
		}

		if friendReq == nil {
			return nil, &errors.Invalid{errors.Base{"You have no connections", false}}
		}

		if friendReq.ToUserID != userID && friendReq.FromUserID != userID {
			return nil, &errors.Invalid{errors.Base{"User can not perform this action", false}}
		}

		if friendReq.Status != model.BlockedFriendStatus {
			//fmt.Println()
			return nil, &errors.Invalid{errors.Base{"wrong way to change this connection", false}}
		}

		if friendReq.BlockerID != userID {
			//fmt.Println()
			return nil, &errors.Invalid{errors.Base{"This user can't unblock this friendship", false}}
		}

		if err := storeFriendRequests.Unblock(id); err != nil {
			logrus.WithField("err", err).Error("unable to unblock friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"invalid request", false}}
		}
		return &dto.BaseResponse{"Unblocked", true}, nil
	}

	return UnblockerFunc(f)
}
