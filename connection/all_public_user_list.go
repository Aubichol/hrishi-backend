package connection

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"go.uber.org/dig"
)

//allPublicUserList hold store handler for getting all users
type allPublicUserList struct {
	storeUsers storeuser.Users
}

//Users implements AllPublicUserList interface
func (p *allPublicUserList) Users(skip, limit int) ([]*dto.PublicUser, error) {

	users, err := p.storeUsers.AllPublic()
	if err != nil {
		logrus.Error("Could not get all users err: ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	allusers := []*dto.PublicUser{}
	for _, user := range users {
		curuser := dto.PublicUser{}
		curuser.FromModelUser(user)
		allusers = append(allusers, &curuser)
	}

	return allusers, nil
}

//PublicUserListParams lists params for NewUserList
type PublicUserListParams struct {
	dig.In
	StoreUsers storeuser.Users
}

//NewPublicUserList provides all public user's list
func NewPublicUserList(params PublicUserListParams) PublicUserList {
	return &allPublicUserList{
		storeUsers: params.StoreUsers,
	}
}
