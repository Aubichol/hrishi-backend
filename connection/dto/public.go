package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//PublicUser holds json type user data
type PublicUser struct {
	FirstName   string                 `json:"first_name"`
	LastName    string                 `json:"last_name"`
	Gender      string                 `json:"gender"`
	ProfilePic  string                 `json:"profile_pic"`
	DateOfBirth MyBirthDate            `json:"date_of_birth"`
	Profile     map[string]interface{} `json:"profile"`
}

type MyBirthDate struct {
	Year  int
	Month int
	Day   int
}

//FromModelUser converts model user to json type user
func (f *PublicUser) FromModelUser(user *model.User) {
	f.FirstName = user.FirstName
	f.LastName = user.LastName
	f.Gender = user.Gender
	f.DateOfBirth.Year = user.BirthDate.Year
	f.DateOfBirth.Month = user.BirthDate.Month
	f.DateOfBirth.Day = user.BirthDate.Day
	f.Profile = user.Profile

	if len(user.ProfilePic) > 0 {
		f.ProfilePic = "/api/v1/pictures/" + user.ProfilePic
	}
}
