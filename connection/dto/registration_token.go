package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
)

type RegistrationToken struct {
	Token  string `json:"token"`
	UserID string `json:"user_id"`
}

func (f *RegistrationToken) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(f)
	if err == nil {
		return nil
	}

	return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
		Base: errors.Base{"invalid registration token", false},
	})
}
