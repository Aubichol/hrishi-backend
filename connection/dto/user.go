package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//User holds json type user data
type User struct {
	ID          string                 `json:"id"`
	FirstName   string                 `json:"first_name"`
	LastName    string                 `json:"last_name"`
	Gender      string                 `json:"gender"`
	ProfilePic  string                 `json:"profile_pic"`
	DateOfBirth BirthDate              `json:"date_of_birth"`
	Profile     map[string]interface{} `json:"profile"`
}

//BirthDate holds json type birth date data
type BirthDate struct {
	Year  int
	Month int
	Day   int
}

//FromModelUser converts model user to json type user
func (f *User) FromModelUser(user *model.User) {
	f.ID = user.ID
	f.FirstName = user.FirstName
	f.LastName = user.LastName
	f.Gender = user.Gender
	f.DateOfBirth.Year = user.BirthDate.Year
	f.DateOfBirth.Month = user.BirthDate.Month
	f.DateOfBirth.Day = user.BirthDate.Day
	f.Profile = user.Profile

	if len(user.ProfilePic) > 0 {
		f.ProfilePic = "/api/v1/pictures/" + user.ProfilePic
	}
}
