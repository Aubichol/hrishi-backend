package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//Friend holds the json data for friend requests
type Friend struct {
	RequestID  string `json:"request_id"`
	ID         string `json:"id"`
	FirstName  string `json:"first_name"`
	LastName   string `json:"last_name"`
	Gender     string `json:"gender"`
	Email      string `json:"email"`
	ProfilePic string `json:"profile_pic"`
}

//FromModelUser converts model data to json data
func (f *Friend) FromModelUser(user *model.User) {
	f.ID = user.ID
	f.FirstName = user.FirstName
	f.LastName = user.LastName
	f.Gender = user.Gender
	f.Email = user.Email

	if len(user.ProfilePic) > 0 {
		f.ProfilePic = "/api/v1/pictures/" + user.ProfilePic
	}
}
