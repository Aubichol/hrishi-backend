package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
)

type FriendRequest struct {
	ID     *string `json:"id,omitempty"`
	Action string  `json:"action"`
}

func (f *FriendRequest) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(f)
	if err == nil {
		return nil
	}

	return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
		Base: errors.Base{"invalid login data", false},
	})
}
