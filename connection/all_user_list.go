package connection

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"go.uber.org/dig"
)

//allUserList hold store handler for getting all users
type allUserList struct {
	storeUsers storeuser.Users
}

//Users implements UserList interface
func (p *allUserList) Users(userID string, skip, limit int) ([]*dto.User, error) {

	users, err := p.storeUsers.All(userID)
	if err != nil {
		logrus.Error("Could not get all users err: ", err)
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	allusers := []*dto.User{}
	for _, user := range users {
		curuser := dto.User{}
		curuser.FromModelUser(user)
		allusers = append(allusers, &curuser)
	}

	return allusers, nil
}

//UserListParams lists params for NewUserList
type UserListParams struct {
	dig.In
	StoreUsers storeuser.Users
}

//NewUserList provides all user's list
func NewUserList(params UserListParams) UserList {
	return &allUserList{
		storeUsers: params.StoreUsers,
	}
}
