package connection

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storefriendreq "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
)

//Blocker provides interface for blocking a user
type Blocker interface {
	Block(id, userID string) (*dto.BaseResponse, error)
}

//BlockerFunc is a type that implements Blocker interface
type BlockerFunc func(id, userID string) (*dto.BaseResponse, error)

//Block implements Blocker interface
func (r BlockerFunc) Block(id, userID string) (*dto.BaseResponse, error) {
	return r(id, userID)
}

//NewBlocker provides Blocker
func NewBlocker(storeFriendRequests storefriendreq.FriendRequests) Blocker {
	f := func(id, userID string) (*dto.BaseResponse, error) {
		friendReq, err := storeFriendRequests.GetByID(id)
		if err != nil {
			logrus.WithField("err", err).Error("unable to get friendreqeust by id ", id)
			return nil, fmt.Errorf("%s:%w", err.Error(),
				&errors.Unknown{errors.Base{"invalid request", false}},
			)
		}

		if friendReq == nil {
			return nil, &errors.Invalid{errors.Base{"You have no connections", false}}
		}

		if friendReq.ToUserID != userID && friendReq.FromUserID != userID {
			return nil, &errors.Invalid{errors.Base{"wrong way to change this connection", false}}
		}

		if friendReq.Status != model.AcceptedFriendStatus {
			return nil, &errors.Invalid{errors.Base{string(friendReq.Status) + " " + string(model.BlockedFriendStatus) + " " + "wrong way to change this connection", false}}
		}

		if err := storeFriendRequests.Block(id, userID); err != nil {
			logrus.WithField("err", err).Error("Unable to block friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
		}
		return &dto.BaseResponse{"Blocked", true}, nil
	}

	return BlockerFunc(f)
}
