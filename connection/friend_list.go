package connection

import "gitlab.com/Aubichol/hrishi-backend/connection/dto"

//FriendList is ...
type FriendList interface {
	Friends(userID string, skip, limit int) ([]*dto.Friend, error)
}
