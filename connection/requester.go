package connection

import (
	"time"

	"github.com/codeginga/locevt"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/event"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storefriendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	"gitlab.com/Aubichol/hrishi-backend/ws"
	"go.uber.org/dig"
)

type Requester interface {
	OpenFriendRequest(fromID, toID string) (*dto.BaseResponse, error)
}

type requester struct {
	storeFriendRequests storefriendrequest.FriendRequests
	event               locevt.Event
}

func (f *requester) openWithPreRequest(preFriendReq *model.FriendRequest) (*dto.BaseResponse, error) {
	// whether rejection will allow same request to be sent again will be decided soon.
	if preFriendReq.Status != model.RejectedFriendStatus {
		return nil, &errors.Invalid{errors.Base{"You can not send friend request to this user", false}}
	}

	preFriendReq.Status = model.PendingFriendStatus
	if err := f.storeFriendRequests.Save(preFriendReq); err != nil {
		logrus.WithFields(logrus.Fields{
			"err": err,
			"id":  preFriendReq.ID,
		}).Error("unable to update friend request update")

		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	logrus.WithField("id", preFriendReq.ID).Debug("friend request status updated")
	f.fireEvent(preFriendReq.ToUserID)
	return &dto.BaseResponse{"Friend request created", true}, nil
}

func (f *requester) fireEvent(toUserID string) {
	dto := ws.FriendRequestDTO{
		Event:  "friend_request",
		Action: "new_request",
	}

	bytes, err := dto.ToBytes()
	if err != nil {
		logrus.Error("tobytes: ", err)
		return
	}

	eventData := event.WSNotificationData{
		UserID: toUserID,
		Data:   bytes,
	}

	f.event.Fire(locevt.FireOption{
		Name:      event.NameWSNotification,
		Data:      &eventData,
		RetryWait: time.Second * 2,
		MaxRetry:  1,
	})
}

func (f *requester) newFriendRequest(fromID, toID string) (*dto.BaseResponse, error) {
	now := time.Now()
	friendRequest := model.FriendRequest{
		FromUserID: fromID,
		ToUserID:   toID,
		UniqueTag:  tag.Unique(fromID, toID),
		Status:     model.PendingFriendStatus,
		CreatedAt:  now,
		UpdatedAt:  now,
	}

	if err := f.storeFriendRequests.Save(&friendRequest); err != nil {
		logrus.WithFields(logrus.Fields{
			"unique tad": friendRequest.UniqueTag,
			"err":        err,
		}).Error("unable to create friend request")

		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	logrus.WithField("id", friendRequest.ID).Debug("friend request created")
	f.fireEvent(toID)
	return &dto.BaseResponse{"Friend request created", true}, nil
}

func (f *requester) OpenFriendRequest(fromID, toID string) (*dto.BaseResponse, error) {
	uniqueTag := tag.Unique(fromID, toID)
	preFriendReq, err := f.storeFriendRequests.FindByUniqueTag(uniqueTag)
	if err != nil {
		logrus.WithFields(logrus.Fields{
			"unique tag": uniqueTag,
			"err":        err,
		}).Error("to get friend request by unique tag")
		return nil, &errors.Unknown{errors.Base{"Invalid request", false}}
	}

	if preFriendReq != nil {
		return f.openWithPreRequest(preFriendReq)
	}

	return f.newFriendRequest(fromID, toID)
}

//NewRequesterParams lists NewRequester dependencies in a struct
type NewRequesterParams struct {
	dig.In
	StoreFriendRequests storefriendrequest.FriendRequests
	Event               locevt.Event
}

//NewRequester provides a Requester
func NewRequester(params NewRequesterParams) Requester {
	return &requester{
		storeFriendRequests: params.StoreFriendRequests,
		event:               params.Event,
	}
}
