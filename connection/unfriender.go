package connection

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
)

type Unfriender interface {
	Unfriend(id, userID string) (*dto.BaseResponse, error)
}

type UnfrienderFunc func(id, userID string) (*dto.BaseResponse, error)

func (r UnfrienderFunc) Unfriend(id, userID string) (*dto.BaseResponse, error) {
	return r(id, userID)
}

func NewUnfriender(storeFriendRequests friendrequest.FriendRequests) Unfriender {
	f := func(id, userID string) (*dto.BaseResponse, error) {
		friendReq, err := storeFriendRequests.GetByID(id)
		if err != nil {
			logrus.WithField("err", err).Error("unable to get friendreqeust by id ", id)
			return nil, fmt.Errorf("%s:%w", err.Error(),
				&errors.Unknown{errors.Base{"invalid request", false}},
			)
		}

		if friendReq == nil {
			return nil, &errors.Invalid{errors.Base{"You have no connections", false}}
		}

		if friendReq.ToUserID != userID && friendReq.FromUserID != userID {
			return nil, &errors.Invalid{errors.Base{"A third user is trying to un-friend others", false}}
		}

		if friendReq.Status != model.AcceptedFriendStatus {
			//fmt.Println()
			return nil, &errors.Invalid{errors.Base{string(friendReq.Status) + " " + string(model.BlockedFriendStatus) + " " + "wrong way to change this connection", false}}
		}

		if err := storeFriendRequests.Unfriend(id); err != nil {
			logrus.WithField("err", err).Error("unable to unfriend friendreqeust by id ", id)
			return nil, &errors.Unknown{errors.Base{"invalid request", false}}
		}
		return &dto.BaseResponse{"Unfriended", true}, nil
	}

	return UnfrienderFunc(f)
}
