package connection

import "gitlab.com/Aubichol/hrishi-backend/connection/dto"

//UserList defines an interface for providing user lists
type UserList interface {
	Users(userID string, skip, limit int) ([]*dto.User, error)
}
