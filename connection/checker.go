package connection

import (
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/cache"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storefriendreq "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
)

//Checker ...
type Checker interface {
	Check(userID1, userID2 string) (bool, error)
}

type checker struct {
	cache               cache.ConnectionStatus
	storeFriendRequests storefriendreq.FriendRequests
}

func (c *checker) Check(userID1, userID2 string) (bool, error) {
	status, err := c.cache.Has(userID1, userID2)
	if err == nil {
		return status, nil
	}

	friendReq, err := c.storeFriendRequests.FindByUniqueTag(tag.Unique(userID1, userID2))
	if err != nil {
		return false, fmt.Errorf("unable to check friend status from db:%w", err)
	}

	status = false
	if friendReq != nil && friendReq.Status == model.AcceptedFriendStatus {
		status = true
	}

	if err := c.cache.Set(userID1, userID2, status); err != nil {
		logrus.Error(err)
	}

	return status, nil
}

func NewChecker(
	cache cache.ConnectionStatus,
	storeFriendRequests storefriendreq.FriendRequests,
) Checker {
	return &checker{cache, storeFriendRequests}
}
