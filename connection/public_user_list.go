package connection

import "gitlab.com/Aubichol/hrishi-backend/connection/dto"

//PublicUserList defines an interface for providing user lists
type PublicUserList interface {
	Users(skip, limit int) ([]*dto.PublicUser, error)
}
