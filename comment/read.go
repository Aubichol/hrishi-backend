package comment

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	"gitlab.com/Aubichol/hrishi-backend/store/comment"
	"gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	"go.uber.org/dig"
)

//Reader provides an interface for reading comments
type Reader interface {
	Read(*dto.ReadReq) (*dto.ReadResp, error)
}

//commentReader implements Reader interface
type commentReader struct {
	comments comment.Comments
	friends  friendrequest.FriendRequests
}

func (read *commentReader) askStore(commentID string) (
	modelComment *model.Comment,
	err error,
) {
	modelComment, err = read.comments.FindByID(commentID)
	return
}

func (read *commentReader) printLog(message string, err error) {
	logrus.Error(
		message,
		err,
	)
}

func (read *commentReader) giveError() (err error) {
	return &errors.Unknown{
		errors.Base{"Invalid request", false},
	}
}

func (read *commentReader) prepareResponse(
	comment *model.Comment,
) (
	resp dto.ReadResp,
) {
	resp.FromModel(comment)
	return
}

func (read *commentReader) AskStoreForRequest(
	GiverID string,
	UserID string,

) (
	currentRequest *model.FriendRequest,
	err error,
) {
	uniqueTag := tag.Unique(GiverID, UserID)
	currentRequest, err = read.friends.FindByUniqueTag(uniqueTag)
	return
}

func (read *commentReader) friendshipExists(status string) (
	exists bool,
) {
	if status == "accepted" {
		exists = true
	} else {
		exists = false
	}
	return
}

func (read *commentReader) isSameUser(
	GiverID string,
	UserID string,
) (
	isSame bool,
) {
	if GiverID == UserID {
		isSame = true
	}
	return
}

func (read *commentReader) giveSuccessfulResponse(
	resp dto.ReadResp,
) (
	*dto.ReadResp, error,
) {
	return &resp, nil
}

func (read *commentReader) giveErrorResponse(err error) (
	*dto.ReadResp,
	error,
) {
	return nil, err
}

func (read *commentReader) errorExists(err error) (exists bool) {
	if err != nil {
		exists = true
	}
	return
}

//Read implements Reader interface
func (read *commentReader) Read(
	commentReq *dto.ReadReq,
) (*dto.ReadResp, error) {
	comment, err := read.askStore(commentReq.CommentID)
	if read.errorExists(err) {
		message := "Could not find status error : "
		read.printLog(message, err)
		return read.giveErrorResponse(read.giveError())
	}

	var resp dto.ReadResp
	resp = read.prepareResponse(comment)
	giverID := comment.UserID

	if read.isSameUser(giverID, commentReq.UserID) {
		return read.giveSuccessfulResponse(resp)
	}

	currentRequest, err := read.AskStoreForRequest(
		giverID,
		comment.UserID,
	)
	if read.errorExists(err) {
		message := "Could not find friendship error : "
		read.printLog(message, err)
		return read.giveErrorResponse(read.giveError())
	}

	if read.friendshipExists(string(currentRequest.Status)) {
		return read.giveErrorResponse(err)
	}
	return read.giveSuccessfulResponse(resp)
}

//NewReaderParams lists params for the NewReader
type NewReaderParams struct {
	dig.In
	Comment comment.Comments
	Friends friendrequest.FriendRequests
}

//NewRead provides Reader
func NewRead(params NewReaderParams) Reader {
	return &commentReader{
		comments: params.Comment,
		friends:  params.Friends,
	}
}
