package comment

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/store/comment"
	"go.uber.org/dig"
)

//CountByStatusReader provides an interface for counting
//comments of a status
type CountByStatusReader interface {
	Count(*dto.CountByStatusReq) (*dto.CountResp, error)
}

//countByUserReader implements Reader interface
type countByStatusReader struct {
	comments comment.Comments
}

func (count *countByStatusReader) askStore(StatusID string) (
	counts int64,
	err error,
) {
	counts, err = count.comments.CountByStatusID(
		StatusID,
	)
	return
}

func (count *countByStatusReader) logError(
	message string,
	err error,
) {
	logrus.Error(message, err)
}

func (count *countByStatusReader) giveError() (err error) {
	return &errors.Unknown{
		errors.Base{"Invalid request", false},
	}
}

func (count *countByStatusReader) prepareResopnse(counts int64) (
	resp dto.CountResp,
) {
	resp.FromModel(counts)
	return
}

func (count *countByStatusReader) giveResponse(
	counts dto.CountResp,
) (
	*dto.CountResp,
	error,
) {
	return &counts, nil
}

func (count *countByStatusReader) giveErrorResponse(err error) (
	*dto.CountResp,
	error,
) {
	return nil, err
}

//Count implements CountByStatusReader interface
func (count *countByStatusReader) Count(
	countByStatusReq *dto.CountByStatusReq,
) (*dto.CountResp, error) {

	counts, err := count.askStore(
		countByStatusReq.StatusID,
	)
	if err != nil {
		message := "Could not count comment by status id error : "
		count.logError(message, err)
		err = count.giveError()
		return count.giveErrorResponse(err)
	}

	var resp dto.CountResp
	resp = count.prepareResopnse(counts)
	return count.giveResponse(resp)
}

//NewCountByStatusReaderParams lists params for the
//NewCountByStatusReader
type NewCountByStatusReaderParams struct {
	dig.In
	Comment comment.Comments
}

//NewCountByStatusReader provides CountByStatusReader
func NewCountByStatusReader(
	params NewByStatusReaderParams,
) CountByStatusReader {
	return &countByStatusReader{
		comments: params.Comment,
	}
}
