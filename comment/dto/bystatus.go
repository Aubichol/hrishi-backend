package dto

//ReadByStatusReq helps gather the data for reading comments by user
type ReadByStatusReq struct {
	StatusID string
}
