package dto

//ReadReq stores comment read request data
type ReadReq struct {
	UserID    string
	CommentID string
}
