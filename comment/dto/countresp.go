package dto

import "strconv"

//CountResp holds the response data for counting comments
type CountResp struct {
	CommentCount string `json:"comment_count"`
}

//FromModel converts the model data to response data
func (r *CountResp) FromModel(commentcnt int64) {
	r.CommentCount = strconv.FormatInt(commentcnt, 10)
}
