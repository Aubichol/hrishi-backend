package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Comment provides dto for user status comment
type Comment struct {
	ID       string `json:"comment_id"`
	Comment  string `json:"comment"`
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
}

//Validate validates comment data
func (c *Comment) Validate(validate *validator.Validate) error {
	if err := validate.Struct(c); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid data", false},
			},
		)
	}
	return nil
}

//FromReader reads comment (?) from request body
func (c *Comment) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(c)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid comment data", false},
		})
	}

	return nil
}
