package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadByStatusResp holds the response data for reading comments
type ReadByStatusResp struct {
	Comments []Comment `json:"comments"`
}

//FromModel converts the model data to response data
func (r *ReadByStatusResp) FromModel(comment []*model.Comment) {
	var curComment Comment
	for _, val := range comment {
		curComment.Comment = val.Comment
		curComment.StatusID = val.StatusID
		curComment.UserID = val.UserID
		curComment.ID = val.ID
		r.Comments = append(r.Comments, curComment)
	}
}
