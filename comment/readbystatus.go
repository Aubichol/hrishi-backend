package comment

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/store/comment"
	"go.uber.org/dig"
)

//ByStatusReader provides an interface for reading statuses
type ByStatusReader interface {
	Read(*dto.ReadByStatusReq, int64, int64) (
		*dto.ReadByStatusResp, error,
	)
}

//statusByUserReader implements Reader interface
type commentsByStatusReader struct {
	comments comment.Comments
}

func (read *commentsByStatusReader) askStore(
	statusID string,
	skip int64,
	limit int64,
) (
	modelComments []*model.Comment,
	err error,
) {
	modelComments, err = read.comments.FindByStatusID(
		statusID,
		skip,
		limit,
	)
	return
}

func (read *commentsByStatusReader) printLog(err error) {
	logrus.Error(
		"Could not find comment by status id error : ",
		err,
	)
}

func (read *commentsByStatusReader) giveError() (err error) {
	return &errors.Unknown{
		errors.Base{"Invalid request", false},
	}
}

func (read *commentsByStatusReader) prepareResponse(
	modelComments []*model.Comment,
) (
	resp dto.ReadByStatusResp,
) {
	resp.FromModel(modelComments)
	return
}

func (read *commentsByStatusReader) errorExists(err error) (
	exists bool,
) {
	if err != nil {
		exists = true
	}
	return true
}

func (read *commentsByStatusReader) giveErrorResponse(err error) (
	*dto.ReadByStatusResp,
	error,
) {
	return nil, err
}

func (read *commentsByStatusReader) giveResponse(
	resp dto.ReadByStatusResp,
) (
	*dto.ReadByStatusResp,
	error,
) {
	return &resp, nil
}

//CommentRead implements ByStatusReader interface
func (read *commentsByStatusReader) Read(
	commentsByStatusReq *dto.ReadByStatusReq,
	skip int64,
	limit int64,
) (
	*dto.ReadByStatusResp, error,
) {

	comments, err := read.askStore(
		commentsByStatusReq.StatusID,
		skip,
		limit,
	)

	if read.errorExists(err) {
		read.printLog(err)
		return read.giveErrorResponse(read.giveError())
	}

	var resp dto.ReadByStatusResp
	resp = read.prepareResponse(comments)
	return read.giveResponse(resp)
}

//NewByStatusReaderParams lists params for the NewReader
type NewByStatusReaderParams struct {
	dig.In
	Comment comment.Comments
}

//NewByStatusReader provides Reader
func NewByStatusReader(
	params NewByStatusReaderParams,
) ByStatusReader {
	return &commentsByStatusReader{
		comments: params.Comment,
	}
}
