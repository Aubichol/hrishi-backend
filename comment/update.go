package comment

import (
	"fmt"
	"time"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storecomment "gitlab.com/Aubichol/hrishi-backend/store/comment"
	"gopkg.in/go-playground/validator.v9"
)

//Updater provides an interface for updating comments
type Updater interface {
	Update(*dto.Update) (*dto.UpdateResponse, error)
}

// update updates user comments
type update struct {
	storeComment storecomment.Comments
	validate     *validator.Validate
}

func (u *update) toModel(usercomment *dto.Update) (
	comment *model.Comment,
) {
	comment = &model.Comment{}
	comment.CreatedAt = time.Now().UTC()
	comment.UpdatedAt = comment.CreatedAt
	comment.Comment = usercomment.Comment
	comment.UserID = usercomment.UserID
	comment.ID = usercomment.CommentID
	return
}

func (u *update) validateData(update *dto.Update) (err error) {
	err = update.Validate(u.validate)
	return
}

func (u *update) convertData(update *dto.Update) (
	modelComment *model.Comment,
) {
	modelComment = u.toModel(update)
	return
}

func (u *update) askStore(modelComment *model.Comment) (
	id string,
	err error,
) {
	id, err = u.storeComment.Save(modelComment)
	return
}

func (u *update) printLogOnSuccess(UserID string) {
	logrus.WithFields(logrus.Fields{
		"id": UserID,
	}).Debug("User updated comment successfully")

}

func (u *update) prepareResponse(
	modelComment *model.Comment, id string,
) *dto.UpdateResponse {
	return &dto.UpdateResponse{
		Message:    "comment updated",
		OK:         true,
		ID:         id,
		UpdateTime: modelComment.UpdatedAt.String(),
	}
}

func (u *update) giveError() (err error) {
	errResp := errors.Unknown{
		Base: errors.Base{
			OK:      false,
			Message: "invalid data",
		},
	}

	err = fmt.Errorf("%s %w", err.Error(), &errResp)
	return
}

func (u *update) giveResponse(resp *dto.UpdateResponse) (
	*dto.UpdateResponse,
	error,
) {
	return resp, nil
}

func (u *update) logError(message string, err error) {
	logrus.Error(message, err)
}

func (u *update) giveErrorResponse(err error) (
	*dto.UpdateResponse,
	error,
) {
	return nil, err
}

//Update implements Update interface
func (u *update) Update(
	update *dto.Update,
) (*dto.UpdateResponse, error) {
	if err := u.validateData(update); err != nil {
		return u.giveErrorResponse(err)
	}

	var modelComment *model.Comment
	modelComment = u.convertData(update)

	id, err := u.askStore(modelComment)
	if err == nil {
		u.printLogOnSuccess(modelComment.UserID)
		var resp *dto.UpdateResponse
		resp = u.prepareResponse(modelComment, id)
		return u.giveResponse(resp)
	}

	message := "Could not update comment "
	u.logError(message, err)

	return u.giveErrorResponse(u.giveError())
}

//NewUpdate returns new instance of Updater
func NewUpdate(
	store storecomment.Comments, validate *validator.Validate,
) Updater {
	return &update{
		store,
		validate,
	}
}
