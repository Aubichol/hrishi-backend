package mongo

import (
	"context"
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/model"
	storestatus "gitlab.com/Aubichol/hrishi-backend/store/status"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/status/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

//users handles user related database queries
type statuses struct {
	c *mongo.Collection
}

// Save saves status from model to database
func (s *statuses) Save(modelStatus *model.Status) (string, error) {
	mongoStatus := mongoModel.Status{}
	if err := mongoStatus.FromModel(modelStatus); err != nil {
		return "", fmt.Errorf("Could not convert model status to mongo status: %w", err)
	}

	if modelStatus.ID == "" {
		mongoStatus.ID = primitive.NewObjectID()
	}

	filter := bson.M{"_id": mongoStatus.ID}
	update := bson.M{"$set": mongoStatus}
	upsert := true

	_, err := s.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return mongoStatus.ID.Hex(), err
}

//FindByID finds a status by id
func (s *statuses) FindByID(id string) (*model.Status, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"_id": objectID}
	result := s.c.FindOne(context.Background(), filter, &options.FindOneOptions{})
	if err := result.Err(); err != nil {
		return nil, err
	}

	status := mongoModel.Status{}
	if err := result.Decode(&status); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return status.ModelStatus(), nil
}

//FindByUser finds a status by id
func (s *statuses) FindByUser(id string, skip int64, limit int64) ([]*model.Status, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"user_id": objectID}

	findOptions := options.Find()
	findOptions.SetSort(map[string]int{"updated_at": -1})
	findOptions.SetSkip(skip)
	findOptions.SetLimit(limit)
	cursor, err := s.c.Find(context.Background(), filter, findOptions)

	if err != nil {
		return nil, err
	}

	return s.cursorToStatuses(cursor)
}

//FindByIDs returns all the statuses from multiple status ids
func (s *statuses) FindByIDs(ids ...string) ([]*model.Status, error) {
	objectIDs := []primitive.ObjectID{}
	for _, id := range ids {
		objectID, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, fmt.Errorf("Invalid id %s : %w", id, err)
		}

		objectIDs = append(objectIDs, objectID)
	}

	filter := bson.M{
		"_id": bson.M{
			"$in": objectIDs,
		},
	}

	cursor, err := s.c.Find(context.Background(), filter, nil)
	if err != nil {
		return nil, err
	}

	return s.cursorToStatuses(cursor)
}

//Search search for users given the text, skip and limit
func (s *statuses) Search(text string, skip, limit int64) ([]*model.Status, error) {
	filter := bson.M{"$text": bson.M{"$search": text}}
	cursor, err := s.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return s.cursorToStatuses(cursor)
}

//cursorToUsers decodes users one by one from the search result
func (s *statuses) cursorToStatuses(cursor *mongo.Cursor) ([]*model.Status, error) {
	defer cursor.Close(context.Background())
	modelStatuses := []*model.Status{}

	for cursor.Next(context.Background()) {
		status := mongoModel.Status{}
		if err := cursor.Decode(&status); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelStatuses = append(modelStatuses, status.ModelStatus())
	}

	return modelStatuses, nil
}

// Params provides parameters for user specific Collection
type Params struct {
	dig.In
	Collection *mongo.Collection `name:"statuses"`
}

//Store provides store for users
func Store(params Params) storestatus.Status {
	return &statuses{params.Collection}
}
