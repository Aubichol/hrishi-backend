package model

import (
	"time"

	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Status defines mongodb data type for Status
type Status struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	Status    string             `bson:"status"`
	UserID    primitive.ObjectID `bson:"user_id"`
	CreatedAt time.Time          `bson:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at"`
}

//FromModel converts model data to mongodb model data for status
func (s *Status) FromModel(modelStatus *model.Status) error {
	s.Status = modelStatus.Status
	s.CreatedAt = modelStatus.CreatedAt
	s.UpdatedAt = modelStatus.UpdatedAt
	var err error
	s.UserID, err = primitive.ObjectIDFromHex(modelStatus.UserID)

	if err != nil {
		return err
	}

	if modelStatus.ID == "" {
		return nil
	}

	id, err := primitive.ObjectIDFromHex(modelStatus.ID)
	if err != nil {
		return err
	}

	s.ID = id
	return nil
}

//ModelStatus converts bson to model for status
func (s *Status) ModelStatus() *model.Status {
	status := model.Status{}
	status.ID = s.ID.Hex()
	status.Status = s.Status
	status.UserID = s.UserID.Hex()
	status.CreatedAt = s.CreatedAt
	status.UpdatedAt = s.UpdatedAt

	return &status
}
