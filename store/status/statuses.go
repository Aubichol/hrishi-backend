package status

import "gitlab.com/Aubichol/hrishi-backend/model"

// Status wraps status's store functionality
type Status interface {
	Save(*model.Status) (id string, err error)
	FindByID(id string) (*model.Status, error)
	FindByUser(id string, skip int64, limit int64) ([]*model.Status, error)
	FindByIDs(id ...string) ([]*model.Status, error)
	//	GiveNewsFeed(id string) ([]*model.Status, error)
	Search(q string, skip, limit int64) ([]*model.Status, error)
}
