package picture

import "gitlab.com/Aubichol/hrishi-backend/model"

//Pictures wrap picture's store functionality
type Pictures interface {
	Save(*model.Picture) error
	Delete(*model.Picture) error
	GetByUserID(userID string, skip, limit int64) ([]*model.Picture, error)
}
