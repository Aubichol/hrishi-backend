package model

import (
	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Picture defines mongodb data type for Picture
type Picture struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	UserID    string             `bson:"user_id"`
	PictureID string             `bson:"picture_id"`
}

//FromModel converts model data to mongodb model data for picture
func (p *Picture) FromModel(data *model.Picture) {
	p.UserID = data.UserID
	p.PictureID = data.PictureID
}

//ModelPicture converts bson to model for picture
func (p *Picture) ModelPicture() *model.Picture {
	data := &model.Picture{}
	data.UserID = p.UserID
	data.PictureID = p.PictureID
	return data
}
