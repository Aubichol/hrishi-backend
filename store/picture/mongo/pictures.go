package mongo

import (
	"context"
	"fmt"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storepicture "gitlab.com/Aubichol/hrishi-backend/store/picture"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/picture/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

//pictures handles picture related database queries
type pictures struct {
	c *mongo.Collection
}

//Save always saves a new picture
func (p *pictures) Save(modelPic *model.Picture) error {
	picture := mongoModel.Picture{}
	picture.FromModel(modelPic)
	picture.ID = primitive.NewObjectID()

	_, err := p.c.InsertOne(context.Background(), picture, &options.InsertOneOptions{})
	if err != nil {
		return err
	}

	return nil
}

//GetByUserID fetches all the pictures of an user given his id
func (p *pictures) GetByUserID(userID string, skip, limit int64) ([]*model.Picture, error) {
	filter := bson.M{"user_id": userID}
	cursor, err := p.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	logrus.WithFields(logrus.Fields{
		"user_id": userID,
		"skip":    skip,
		"limit":   limit,
	}).Info("image db list params")

	return p.cursorToPictures(cursor)
}

//Delete deletes a picture given the user's id and the picture's id
func (p *pictures) Delete(data *model.Picture) error {
	filter := bson.M{
		"user_id":    data.UserID,
		"picture_id": data.PictureID,
	}

	_, err := p.c.DeleteOne(context.Background(), filter, nil)
	if err != nil {
		return err
	}

	return nil
}

//cursorToPictures decodes pictures one by one from the search results
func (p *pictures) cursorToPictures(cursor *mongo.Cursor) ([]*model.Picture, error) {
	defer cursor.Close(context.Background())
	modelPictures := []*model.Picture{}

	for cursor.Next(context.Background()) {
		picture := mongoModel.Picture{}
		if err := cursor.Decode(&picture); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelPictures = append(modelPictures, picture.ModelPicture())
	}
	logrus.Info("image len db :", len(modelPictures))
	return modelPictures, nil
}

//PicturesParams provides parameters for pictures specific Collections
type PicturesParams struct {
	dig.In
	Collection *mongo.Collection `name:"pictures"`
}

//Store provides a constructor to the calling dig container
func Store(params PicturesParams) storepicture.Pictures {
	return &pictures{params.Collection}
}
