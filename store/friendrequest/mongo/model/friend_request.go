package model

import (
	"time"

	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type FriendRequest struct {
	ID         primitive.ObjectID `bson:"_id,omitempty"`
	FromUserID primitive.ObjectID `bson:"from_user_id"`
	ToUserID   primitive.ObjectID `bson:"to_user_id"`
	BlockerID  primitive.ObjectID `bson:"blocker_id"`
	UniqueTag  string             `bson:"unique_tag"`
	Status     string             `bson:"status"`
	CreatedAt  time.Time          `bson:"created_at"`
	UpdatedAt  time.Time          `bson:"updated_at"`
}

func (f *FriendRequest) FromModel(modelFriendReq *model.FriendRequest) error {
	f.UniqueTag = modelFriendReq.UniqueTag
	f.Status = string(modelFriendReq.Status)
	f.CreatedAt = modelFriendReq.CreatedAt
	f.UpdatedAt = modelFriendReq.UpdatedAt

	var err error
	if modelFriendReq.ID != "" {
		f.ID, err = primitive.ObjectIDFromHex(modelFriendReq.ID)
		if err != nil {
			return err
		}
	}
	
	if modelFriendReq.BlockerID != "" {
		f.BlockerID, err = primitive.ObjectIDFromHex(modelFriendReq.BlockerID)
		if err != nil {
			return err
		}
	}

	f.FromUserID, err = primitive.ObjectIDFromHex(modelFriendReq.FromUserID)
	if err != nil {
		return err
	}

	f.ToUserID, err = primitive.ObjectIDFromHex(modelFriendReq.ToUserID)
	if err != nil {
		return err
	}

	return nil
}

func (f *FriendRequest) ModelFriendReq() *model.FriendRequest {
	friendReq := model.FriendRequest{}
	friendReq.ID = f.ID.Hex()
	friendReq.FromUserID = f.FromUserID.Hex()
	friendReq.ToUserID = f.ToUserID.Hex()
	friendReq.UniqueTag = f.UniqueTag
	friendReq.Status = model.FriendStatus(f.Status)
	friendReq.CreatedAt = f.CreatedAt
	friendReq.UpdatedAt = f.UpdatedAt
	friendReq.BlockerID = f.BlockerID.Hex()

	return &friendReq
}
