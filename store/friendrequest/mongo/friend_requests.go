package mongo

import (
	"context"
	"fmt"
	"time"

	"gitlab.com/Aubichol/hrishi-backend/model"
	storefriendreq "gitlab.com/Aubichol/hrishi-backend/store/friendrequest"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/friendrequest/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

//friendRequests handles friend requests related structs
type friendRequests struct {
	c *mongo.Collection
}

//Save saves a friend request
func (f *friendRequests) Save(modelFriendReq *model.FriendRequest) error {
	mongoFriendReq := mongoModel.FriendRequest{}
	if err := mongoFriendReq.FromModel(modelFriendReq); err != nil {
		return fmt.Errorf("Could not convert model user to mongo model: %w", err)
	}

	if modelFriendReq.ID == "" {
		mongoFriendReq.ID = primitive.NewObjectID()
	}

	filter := bson.M{"_id": mongoFriendReq.ID}
	update := bson.M{"$set": mongoFriendReq}
	upsert := true

	_, err := f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return err
}

//Unfriend unfriends a friend request
func (f *friendRequests) Unfriend(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{
		"_id":    objectID,
		"status": model.AcceptedFriendStatus,
	}
	update := bson.M{"$set": bson.M{
		"status":     model.RejectedFriendStatus,
		"updated_at": time.Now().UTC(),
	}}

	_, err = f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{},
	)

	return err
}

//Unblock unblocks a previously blocked request
func (f *friendRequests) Unblock(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{
		"_id":    objectID,
		"status": model.BlockedFriendStatus,
	}
	update := bson.M{"$set": bson.M{
		"status":     model.AcceptedFriendStatus,
		"updated_at": time.Now().UTC(),
	}}

	_, err = f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{},
	)

	return err
}

//Block blocks a friendship
func (f *friendRequests) Block(id string, userID string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("Invalid id %s : %w", id, err)
	}
	usrID, errr := primitive.ObjectIDFromHex(userID)
	if errr != nil {
		return fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{
		"_id":    objectID,
		"status": model.AcceptedFriendStatus,
	}
	update := bson.M{"$set": bson.M{
		"status":     model.BlockedFriendStatus,
		"blocker_id": usrID,
		"updated_at": time.Now().UTC(),
	}}

	_, err = f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{},
	)

	return err
}

//Reject function rejects a pending friend request
func (f *friendRequests) Reject(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{
		"_id":    objectID,
		"status": model.PendingFriendStatus,
	}
	update := bson.M{"$set": bson.M{
		"status":     model.RejectedFriendStatus,
		"updated_at": time.Now().UTC(),
	}}

	_, err = f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{},
	)

	return err
}

func (f *friendRequests) Accept(id string) error {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return fmt.Errorf("invalid id %s : %w", id, err)
	}

	filter := bson.M{
		"_id":    objectID,
		"status": model.PendingFriendStatus,
	}
	update := bson.M{"$set": bson.M{
		"status":     model.AcceptedFriendStatus,
		"updated_at": time.Now().UTC(),
	}}

	_, err = f.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{},
	)

	return err
}

//GetByID gives friend request by id
func (f *friendRequests) GetByID(id string) (*model.FriendRequest, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"_id": objectID}
	result := f.c.FindOne(context.Background(), filter, &options.FindOneOptions{})
	if result.Err() == mongo.ErrNoDocuments {
		return nil, nil
	}
	if err := result.Err(); err != nil {
		return nil, err
	}

	friendReq := mongoModel.FriendRequest{}
	if err := result.Decode(&friendReq); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return friendReq.ModelFriendReq(), nil
}

//PendingRequest returns a list of pending requests
func (f *friendRequests) PendingRequests(userID string, skip, limit int64) (
	[]*model.FriendRequest,
	error,
) {
	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, fmt.Errorf("Invalid user id %s : %w", userID, err)
	}

	filter := bson.M{
		"to_user_id": objectID,
		"status":     model.PendingFriendStatus,
	}

	cursor, err := f.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return f.cursorToFriendReqs(cursor)
}

func (f *friendRequests) ActiveFriends(userID string, skip, limit int64) (
	[]*model.FriendRequest,
	error,
) {

	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, fmt.Errorf("Invalid user id %s : %w", userID, err)
	}

	filter := bson.M{
		"$or": bson.A{
			bson.M{"to_user_id": objectID},
			bson.M{"from_user_id": objectID},
		},
		"status": model.AcceptedFriendStatus,
	}

	cursor, err := f.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return f.cursorToFriendReqs(cursor)
}

//BlockedFriends returns a list of blocked friends given the user id
func (f *friendRequests) BlockedFriends(userID string, skip, limit int64) (
	[]*model.FriendRequest,
	error,
) {

	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, fmt.Errorf("Invalid user id %s : %w", userID, err)
	}

	filter := bson.M{
		"$or": bson.A{
			bson.M{"to_user_id": objectID},
			bson.M{"from_user_id": objectID},
		},
		"blocker_id": objectID,
		"status":     model.BlockedFriendStatus,
	}

	cursor, err := f.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return f.cursorToFriendReqs(cursor)
}

//UnfriendFriend unfriends a fried
func (f *friendRequests) UnfriendFriends(userID string, skip, limit int64) (
	[]*model.FriendRequest,
	error,
) {

	objectID, err := primitive.ObjectIDFromHex(userID)
	if err != nil {
		return nil, fmt.Errorf("Invalid user id %s : %w", userID, err)
	}

	filter := bson.M{
		"$or": bson.A{
			bson.M{"to_user_id": objectID},
			bson.M{"from_user_id": objectID},
		},
		"status": model.UnfriendFriendStatus,
	}

	cursor, err := f.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return f.cursorToFriendReqs(cursor)
}

//FindByUniqueTag finds friend request from unique tag
func (f *friendRequests) FindByUniqueTag(tag string) (*model.FriendRequest, error) {
	filter := bson.M{"unique_tag": tag}
	result := f.c.FindOne(context.Background(), filter, &options.FindOneOptions{})
	if result.Err() == mongo.ErrNoDocuments {
		return nil, nil
	}

	if err := result.Err(); err != nil {

		return nil, err
	}

	friendReq := mongoModel.FriendRequest{}
	if err := result.Decode(&friendReq); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return friendReq.ModelFriendReq(), nil
}

func (f *friendRequests) cursorToFriendReqs(cursor *mongo.Cursor) ([]*model.FriendRequest, error) {
	defer cursor.Close(context.Background())
	modelFriendReqs := []*model.FriendRequest{}

	for cursor.Next(context.Background()) {
		friendReq := mongoModel.FriendRequest{}
		if err := cursor.Decode(&friendReq); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelFriendReqs = append(modelFriendReqs, friendReq.ModelFriendReq())
	}

	return modelFriendReqs, nil
}

//Params provides friend request specific paramters to the constructor
type Params struct {
	dig.In
	Collection *mongo.Collection `name:"friend_requests"`
}

//Store provides a constructor to the calling dig container
func Store(params Params) storefriendreq.FriendRequests {
	return &friendRequests{params.Collection}
}
