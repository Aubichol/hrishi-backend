package friendrequest

import "gitlab.com/Aubichol/hrishi-backend/model"

//FriendRequests wraps friend request's store functionality
type FriendRequests interface {
	Save(*model.FriendRequest) error
	Unfriend(id string) error
	Unblock(id string) error
	Block(id string, userID string) error
	Reject(id string) error
	Accept(id string) error
	GetByID(id string) (*model.FriendRequest, error)
	PendingRequests(userID string, skip, limit int64) ([]*model.FriendRequest, error)
	ActiveFriends(userID string, skip, limit int64) ([]*model.FriendRequest, error)
	BlockedFriends(userID string, skip, limit int64) ([]*model.FriendRequest, error)
	FindByUniqueTag(tag string) (*model.FriendRequest, error)
}
