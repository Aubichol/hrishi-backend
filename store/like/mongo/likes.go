package mongo

import (
	"context"
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/model"
	storelike "gitlab.com/Aubichol/hrishi-backend/store/like"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/like/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

//users handles user related database queries
type likes struct {
	c *mongo.Collection
}

// Save saves likes from model to database
func (l *likes) Save(modelLike *model.Like) (string, error) {
	mongoLike := mongoModel.Like{}
	if err := mongoLike.FromModel(modelLike); err != nil {
		return "", fmt.Errorf(
			"Could not convert model comment to mongo comment: %w",
			err,
		)
	}

	if modelLike.ID == "" {
		mongoLike.ID = primitive.NewObjectID()
	}

	filter := bson.M{"_id": mongoLike.ID}
	update := bson.M{"$set": mongoLike}
	upsert := true

	_, err := l.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return mongoLike.ID.Hex(), err
}

//FindByID finds a like by id
func (l *likes) FindByID(id string) (*model.Like, error) {
	objectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"_id": objectID}
	result := l.c.FindOne(context.Background(), filter, &options.FindOneOptions{})
	if err := result.Err(); err != nil {
		return nil, err
	}

	like := mongoModel.Like{}
	if err := result.Decode(&like); err != nil {
		return nil, fmt.Errorf("Could not decode mongo model to model : %w", err)
	}

	return like.ModelLike(), nil
}

//FindByID finds a like by id
func (l *likes) CheckExistence(statusid, giverid string) (bool, error) {
	statusID, err := primitive.ObjectIDFromHex(statusid)
	if err != nil {
		return false, fmt.Errorf("Invalid id %s : %w", statusid, err)
	}

	giverID, err := primitive.ObjectIDFromHex(giverid)
	if err != nil {
		return false, fmt.Errorf("Invalid id %s : %w", giverid, err)
	}
	filter := bson.M{"status_id": statusID, "user_id": giverID}

	result := l.c.FindOne(context.Background(), filter, &options.FindOneOptions{})
	if err := result.Err(); err != nil {
		return false, err
	}

	return true, nil
}

//FindByStatusID finds likes by id
func (l *likes) FindByStatusID(id string, skip int64, limit int64) ([]*model.Like, error) {
	objectID, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return nil, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"status_id": objectID}
	cursor, err := l.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})

	if err != nil {
		return nil, err
	}

	return l.cursorToLikes(cursor)
}

//CountByStatusID counts likes from status id
func (l *likes) CountByStatusID(id string) (int64, error) {
	objectID, err := primitive.ObjectIDFromHex(id)

	if err != nil {
		return -1, fmt.Errorf("Invalid id %s : %w", id, err)
	}

	filter := bson.M{"status_id": objectID}
	cnt, err := l.c.CountDocuments(context.Background(), filter, &options.CountOptions{})

	if err != nil {
		return -1, err
	}

	return cnt, nil
}

//FindByIDs returns all the users from multiple user ids
func (l *likes) FindByIDs(ids ...string) ([]*model.Like, error) {
	objectIDs := []primitive.ObjectID{}
	for _, id := range ids {
		objectID, err := primitive.ObjectIDFromHex(id)
		if err != nil {
			return nil, fmt.Errorf("Invalid id %s : %w", id, err)
		}

		objectIDs = append(objectIDs, objectID)
	}

	filter := bson.M{
		"_id": bson.M{
			"$in": objectIDs,
		},
	}

	cursor, err := l.c.Find(context.Background(), filter, nil)
	if err != nil {
		return nil, err
	}

	return l.cursorToLikes(cursor)
}

//Search searches for users given the text, skip and limit
func (l *likes) Search(text string, skip, limit int64) ([]*model.Like, error) {
	filter := bson.M{"$text": bson.M{"$search": text}}
	cursor, err := l.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})
	if err != nil {
		return nil, err
	}

	return l.cursorToLikes(cursor)
}

//cursorToLikes decodes users one by one from the search result
func (l *likes) cursorToLikes(cursor *mongo.Cursor) ([]*model.Like, error) {
	defer cursor.Close(context.Background())
	modelLikes := []*model.Like{}

	for cursor.Next(context.Background()) {
		like := mongoModel.Like{}
		if err := cursor.Decode(&like); err != nil {
			return nil, fmt.Errorf("Could not decode data from mongo %w", err)
		}

		modelLikes = append(modelLikes, like.ModelLike())
	}

	return modelLikes, nil
}

//Params provides parameters for like specific Collection
type Params struct {
	dig.In
	Collection *mongo.Collection `name:"likes"`
}

//Store provides store for users
func Store(params Params) storelike.Like {
	return &likes{params.Collection}
}
