package model

import (
	"fmt"
	"time"

	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//Like holds the bson type for like
type Like struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	LikeStr   string             `bson:"like_str"`
	UserID    primitive.ObjectID `bson:"user_id"`
	StatusID  primitive.ObjectID `bson:"status_id"`
	Active    bool               `bson:"is_active"`
	CreatedAt time.Time          `bson:"created_at"`
	UpdatedAt time.Time          `bson:"updated_at"`
}

//FromModel converts model data to bson
func (l *Like) FromModel(modelLike *model.Like) error {
	l.LikeStr = modelLike.LikeStr
	l.CreatedAt = modelLike.CreatedAt
	l.UpdatedAt = modelLike.UpdatedAt
	l.Active = modelLike.Active
	var err error
	fmt.Println("Inside from model")
	l.UserID, err = primitive.ObjectIDFromHex(modelLike.UserID)
	fmt.Println("Error user id: ", err)
	l.StatusID, err = primitive.ObjectIDFromHex(modelLike.StatusID)
	fmt.Println("Error status id: ", err)

	if modelLike.ID == "" {
		return nil
	}

	id, err := primitive.ObjectIDFromHex(modelLike.ID)
	if err != nil {
		return err
	}

	l.ID = id
	return nil
}

//ModelLike converts bson to model
func (l *Like) ModelLike() *model.Like {
	like := model.Like{}
	like.ID = l.ID.Hex()
	like.LikeStr = l.LikeStr
	like.UserID = l.UserID.Hex()
	like.StatusID = l.StatusID.Hex()
	like.CreatedAt = l.CreatedAt
	like.UpdatedAt = l.UpdatedAt

	return &like
}
