package like

import "gitlab.com/Aubichol/hrishi-backend/model"

// Like wraps like's store functionality
type Like interface {
	Save(*model.Like) (string, error)
	FindByID(id string) (*model.Like, error)
	FindByStatusID(id string, skip int64, limit int64) ([]*model.Like, error)
	CountByStatusID(id string) (int64, error)
	CheckExistence(statusid, giverid string) (bool, error)
	FindByIDs(id ...string) ([]*model.Like, error)
	Search(q string, skip, limit int64) ([]*model.Like, error)
}
