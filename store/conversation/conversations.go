package conversation

import "gitlab.com/Aubichol/hrishi-backend/model"

//Conversations wraps conversation's store functionality
type Conversations interface {
	Create(*model.Conversation) error
	GetByUniqueTagFromID(id *string, tag string, skip, limit int64) ([]*model.Conversation, error)
	GetByUniqueTagFromIDReverse(id *string, tag string, skip, limit int64) ([]*model.Conversation, error)
}
