package model

import (
	"fmt"
	"time"

	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Conversation struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	UniqueTag string             `bson:"unique_tag"`
	UserID    string             `bson:"user_id"`
	Message   string              `bson:"message"`
	CreatedAt time.Time          `bson:"created_at"`
}

func (c *Conversation) FromModel(data *model.Conversation) error {
	if data.ID != "" {
		objectID, err := primitive.ObjectIDFromHex(data.ID)
		if err != nil {
			return fmt.Errorf("invalid id, err=%s", err.Error())
		}
		c.ID = objectID
	}

	c.UniqueTag = data.UniqueTag
	c.UserID = data.UserID
	c.Message = data.Message
	c.CreatedAt = data.CreatedAt
	return nil
}

func (c *Conversation) ModelConversation() *model.Conversation {
	data := model.Conversation{}
	data.ID = c.ID.Hex()
	data.UniqueTag = c.UniqueTag
	data.UserID = c.UserID
	data.Message = c.Message
	data.CreatedAt = c.CreatedAt
	return &data
}
