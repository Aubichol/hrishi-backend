package model

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

//ReadPointer defines mongodb data type for ConversationReadPointer
type ReadPointer struct {
	ID             primitive.ObjectID `bson:"_id,omitempty"`
	ConversationID string             `bson:"conversation_id"`
	UniqueTag      string             `bson:"unique_tag"`
	UserID         string             `bson:"user_id"`
}

//FromModel converts model data to mongodb model data for conversation read pointer
func (c *ReadPointer) FromModel(data *model.ReadPointer) error {
	if data.ID != "" {
		objectID, err := primitive.ObjectIDFromHex(data.ID)
		if err != nil {
			return fmt.Errorf("invalid id, err=%s", err.Error())
		}
		c.ID = objectID
	}

	c.ConversationID = data.ConversationID
	c.UniqueTag = data.UniqueTag
	c.UserID = data.UserID

	return nil
}

//ModelReadPointer converts bson to model for conversation read pointer
func (c *ReadPointer) ModelReadPointer() *model.ReadPointer {
	data := model.ReadPointer{}
	data.ID = c.ID.Hex()
	data.ConversationID = c.ConversationID
	data.UniqueTag = c.UniqueTag
	data.UserID = c.UserID
	return &data
}
