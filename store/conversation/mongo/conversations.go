package mongo

import (
	"context"
	"fmt"
	"sort"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/conversation/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

type conversations struct {
	c *mongo.Collection
}

//Create ...
func (c *conversations) Create(data *model.Conversation) error {
	conversation := mongoModel.Conversation{}
	if err := conversation.FromModel(data); err != nil {
		return err
	}
	conversation.ID = primitive.NewObjectID()

	result, err := c.c.InsertOne(context.Background(), conversation, &options.InsertOneOptions{})
	if err != nil {
		return fmt.Errorf("could not create conversation err=%s", err)
	}

	data.ID = conversation.ID.Hex()
	logrus.Debug("conversation created ", result.InsertedID)

	return nil
}

func (c *conversations) GetByUniqueTagFromID(id *string, tag string, skip, limit int64) ([]*model.Conversation, error) {
	filter := bson.M{"unique_tag": tag}
	if id != nil {
		objectID, err := primitive.ObjectIDFromHex(*id)
		if err != nil {
			return nil, fmt.Errorf("invalid id, err=%s", err.Error())
		}
		filter["_id"] = bson.M{"$gt": objectID}
	}

	cursor, err := c.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
	})

	if err != nil {
		return nil, fmt.Errorf("could not get conversation, err=%s", err.Error())
	}

	return c.cursorToConversation(cursor)
}

func (c *conversations) GetByUniqueTagFromIDReverse(id *string, tag string, skip, limit int64) ([]*model.Conversation, error) {
	filter := bson.M{"unique_tag": tag}
	if id != nil {
		objectID, err := primitive.ObjectIDFromHex(*id)
		if err != nil {
			return nil, fmt.Errorf("invalid id, err=%s", err.Error())
		}
		filter["_id"] = bson.M{"$lt": objectID}
	}

	cursor, err := c.c.Find(context.Background(), filter, &options.FindOptions{
		Skip:  &skip,
		Limit: &limit,
		Sort:  bson.M{"_id": -1},
	})

	if err != nil {
		return nil, fmt.Errorf("could not get conversation, err=%s", err.Error())
	}

	conversations, err := c.cursorToConversation(cursor)
	if err != nil {
		return nil, err
	}

	sort.Sort(model.Conversations(conversations))
	return conversations, nil
}

func (c *conversations) cursorToConversation(cursor *mongo.Cursor) ([]*model.Conversation, error) {
	defer cursor.Close(context.Background())
	modelConversations := []*model.Conversation{}

	for cursor.Next(context.Background()) {
		conversation := mongoModel.Conversation{}
		if err := cursor.Decode(&conversation); err != nil {
			return nil, fmt.Errorf("could not decode data from mongo %w", err)
		}

		modelConversations = append(modelConversations, conversation.ModelConversation())
	}

	return modelConversations, nil
}

//ConversationsParams lists parameters for NewConversations
type ConversationsParams struct {
	dig.In
	Collection *mongo.Collection `name:"conversations"`
}

//Store provides Conversations
func Store(params ConversationsParams) storeconversation.Conversations {
	return &conversations{params.Collection}
}
