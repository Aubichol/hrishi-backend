package mongo

import (
	"context"
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/model"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	mongoModel "gitlab.com/Aubichol/hrishi-backend/store/conversation/mongo/model"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.uber.org/dig"
)

type readPointers struct {
	c *mongo.Collection
}

func (c *readPointers) Save(conversationID, uniqueTag, userID string) error {

	upsert := true
	filter := bson.M{
		"unique_tag": uniqueTag,
		"user_id":    userID,
	}
	update := bson.M{"$set": bson.M{"conversation_id": conversationID}}

	_, err := c.c.UpdateOne(
		context.Background(),
		filter,
		update,
		&options.UpdateOptions{
			Upsert: &upsert,
		},
	)

	return err
}

func (c *readPointers) GetByUserIDAndTag(userID, uniqueTag string) (*model.ReadPointer, error) {
	filter := bson.M{
		"unique_tag": uniqueTag,
		"user_id":    userID,
	}

	result := c.c.FindOne(context.Background(), filter, &options.FindOneOptions{})

	if result.Err() == mongo.ErrNoDocuments {
		return nil, nil
	}

	if err := result.Err(); err != nil {
		return nil, fmt.Errorf("Could not get conversation read pointer, err=%s", err.Error())
	}

	data := mongoModel.ReadPointer{}
	if err := result.Decode(&data); err != nil {
		return nil, fmt.Errorf("Could not decode data from mongo, err=%s", err.Error())
	}

	return data.ModelReadPointer(), nil
}

//ReadPointersParams lists parameters for NewReadPointers
type ReadPointersParams struct {
	dig.In
	Collection *mongo.Collection `name:"conversation_read_pointers"`
}

//ReadPointersStore returns a provider for ReadPointers
func ReadPointersStore(params ReadPointersParams) storeconversation.ReadPointers {
	return &readPointers{params.Collection}
}
