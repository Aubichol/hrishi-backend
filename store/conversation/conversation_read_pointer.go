package conversation

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadPointers stores conversation read pointers store functionality
type ReadPointers interface {
	Save(conversationID, uniqueTag, userID string) error
	GetByUserIDAndTag(userID, uniqueTag string) (*model.ReadPointer, error)
}
