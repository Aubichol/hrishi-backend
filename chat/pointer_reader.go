package chat

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	"go.uber.org/dig"
)

//PointerReader holds database operations related to latest chats interface
type PointerReader interface {
	PointerRead(*dto.LatestReadReq) (*dto.ReadPointer, error)
}

//latestReader holds database operations related to latest chats handler
type pointerReader struct {
	conversations     storeconversation.Conversations
	readPointers      storeconversation.ReadPointers
	connectionChecker connection.Checker
}

func (l *pointerReader) getPointer(req *dto.LatestReadReq) (
	*model.ReadPointer,
	error,
) {

	uniqueTag := tag.Unique(req.UserID, req.FriendID)
	pointer, err := l.readPointers.GetByUserIDAndTag(req.UserID, uniqueTag)
	if err != nil {
		return nil, fmt.Errorf(
			"Could not get read pointer, err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"Unable to read pointer", false}},
		)
	}

	if pointer != nil {
		return pointer, nil
	}

	pointer = &model.ReadPointer{
		UniqueTag: uniqueTag,
		UserID:    req.UserID,
	}

	return pointer, nil
}

func (l *pointerReader) checkConnection(req *dto.LatestReadReq) error {
	ok, err := l.connectionChecker.Check(req.UserID, req.FriendID)
	if err != nil {
		return fmt.Errorf(
			"Could not check connection error=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"Unable to check friendship status", false}},
		)
	}

	if ok {
		return nil
	}

	return &errors.Invalid{errors.Base{"You are not friends", false}}

}

//LatestRead reads the latest read messages
func (l *pointerReader) PointerRead(req *dto.LatestReadReq) (*dto.ReadPointer, error) {
	if err := l.checkConnection(req); err != nil {
		return nil, err
	}

	pointer, err := l.getPointer(req)
	if err != nil {
		return nil, err
	}

	var resp dto.ReadPointer
	resp.FromModel(pointer)

	return &resp, nil
}

//PointerReaderParams lists parameters for NewLatestReader
type PointerReaderParams struct {
	dig.In
	Conversations     storeconversation.Conversations
	ReadPointers      storeconversation.ReadPointers
	ConnectionChecker connection.Checker
	Cfg               cfg.Limit
}

//NewPointerReader provides a LatestReader
func NewPointerReader(params PointerReaderParams) PointerReader {
	return &pointerReader{
		conversations:     params.Conversations,
		readPointers:      params.ReadPointers,
		connectionChecker: params.ConnectionChecker,
	}
}
