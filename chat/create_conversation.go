package chat

import (
	"fmt"
	"time"

	"github.com/codeginga/locevt"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/event"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	"gitlab.com/Aubichol/hrishi-backend/ws"
	"go.uber.org/dig"
	"gopkg.in/go-playground/validator.v9"
)

//CreateConversation ...
type CreateConversation interface {
	Create(message *dto.Message, fromUserID, toUserID string) (*dto.ChatPost, error)
}

type createConversation struct {
	storeConversations storeconversation.Conversations
	connectionChecker  connection.Checker
	validate           *validator.Validate
	event              locevt.Event
}

func (c *createConversation) checkConnection(id1, id2 string) error {
	ok, err := c.connectionChecker.Check(id1, id2)
	if err != nil {
		return fmt.Errorf(
			"could not check connection err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to check friendship status", false}},
		)
	}

	if ok {
		return nil
	}

	return &errors.Invalid{errors.Base{"you are not friend", false}}

}

func (c *createConversation) fireEvent(toUserID string, data *model.Conversation) {
	chatDTO := ws.ChatDTO{
		ID:      data.ID,
		Message: data.Message,
		UserID:  data.UserID,
		Event:   "chat",
	}

	bytes, err := chatDTO.ToBytes()
	if err != nil {
		logrus.Error("tobytes: ", err)
		return
	}

	eventData := event.WSNotificationData{
		UserID: toUserID,
		Data:   bytes,
	}

	c.event.Fire(locevt.FireOption{
		Name:      event.NameWSNotification,
		Data:      &eventData,
		RetryWait: time.Second * 2,
		MaxRetry:  1,
	})
}

//Create ...
func (c *createConversation) Create(
	message *dto.Message,
	fromUserID,
	toUserID string,
) (*dto.ChatPost, error) {
	if err := message.Validate(c.validate); err != nil {
		return nil, err
	}

	if err := c.checkConnection(fromUserID, toUserID); err != nil {
		return nil, err
	}

	conversation := model.Conversation{
		UniqueTag: tag.Unique(fromUserID, toUserID),
		UserID:    fromUserID,
		Message:   message.Message,
		CreatedAt: time.Now().UTC(),
	}

	if err := c.storeConversations.Create(&conversation); err != nil {
		return nil, fmt.Errorf(
			"could not create conversation at store err=%s:%w",
			err.Error(),
			&errors.Invalid{errors.Base{"There is a problem to create conversation", false}},
		)
	}

	c.fireEvent(toUserID, &conversation)
	c.fireEvent(fromUserID, &conversation)

	resp := dto.ChatPost{}
	resp.FromModelConversation(&conversation)
	return &resp, nil
}

type CreateConversationParams struct {
	dig.In
	Conversations storeconversation.Conversations
	Checker       connection.Checker
	Validate      *validator.Validate
	Event         locevt.Event
}

//NewCreateConversation ...
func NewCreateConversation(params CreateConversationParams) CreateConversation {
	return &createConversation{
		storeConversations: params.Conversations,
		connectionChecker:  params.Checker,
		validate:           params.Validate,
		event:              params.Event,
	}
}
