package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gopkg.in/go-playground/validator.v9"
)

//Direction is a defined type
type Direction string

//Direction constants are defined
const (
	DirectionForward  Direction = "forward"
	DirectionBackword           = "back"
)

//Message stores json type of message
type Message struct {
	Message string `json:"message" validate:"required,max=50000"`
}

//Validate validates message
func (m *Message) Validate(validate *validator.Validate) error {
	if err := validate.Struct(m); err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			errors.Base{"invalid data", false},
		})
	}

	return nil
}

//FromReader reads messages from request
func (m *Message) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(m)
	if err == nil {
		return nil
	}

	return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
		Base: errors.Base{"invalid message body", false},
	})
}

//Response stores json type of message
type Response struct {
	Message string `json:"message"`
}

//ChatPost stores id and message
type ChatPost struct {
	ID      string `json:"id"`
	Message string `json:"message"`
}

//FromModelConversation converts model data to json type data
func (c *ChatPost) FromModelConversation(data *model.Conversation) {
	c.ID = data.ID
	c.Message = data.Message
}

//ReadMessage stores message
type ReadMessage struct {
	Message
	ConversationID string `json:"conversation_id"`
	UserID         string
}

//FromModelConversation ...
func (r *ReadMessage) FromModelConversation(data *model.Conversation) {
	r.Message.Message = data.Message
	r.ConversationID = data.ID
	r.UserID = data.UserID
}

//PreviousReadReq ...
type PreviousReadReq struct {
	UserID         string
	FriendID       string
	ConversationID string
}

//LatestReadReq stores the chat read pointer request
type LatestReadReq struct {
	UserID         string
	FriendID       string
	ConversationID string
}
