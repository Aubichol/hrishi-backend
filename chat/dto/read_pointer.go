package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadPointer holds the json data for chat
type ReadPointer struct {
	ConversationID string `json:"conversation_id"`
	ID             string `json:"id"`
}

//FromModel converts model data to json data
func (f *ReadPointer) FromModel(cr *model.ReadPointer) {
	f.ID = cr.ID
	f.ConversationID = cr.ConversationID

}
