package chat

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	"go.uber.org/dig"
)

//LatestReader holds database operations related to latest chats interface
type LatestReader interface {
	LatestRead(*dto.LatestReadReq) ([]*dto.ReadMessage, error)
}

//latestReader holds database operations related to latest chats handler
type latestReader struct {
	conversations     storeconversation.Conversations
	readPointers      storeconversation.ReadPointers
	connectionChecker connection.Checker
	listLimit         int
}

//getPointer returns the latest conversation id that a particular user have seen so far
func (l *latestReader) getPointer(req *dto.LatestReadReq) (
	*model.ReadPointer,
	error,
) {

	uniqueTag := tag.Unique(req.UserID, req.FriendID)
	pointer, err := l.readPointers.GetByUserIDAndTag(req.UserID, uniqueTag)
	if err != nil {
		return nil, fmt.Errorf(
			"could not get read pointer, err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	if pointer != nil {
		return pointer, nil
	}

	pointer = &model.ReadPointer{
		UniqueTag: uniqueTag,
		UserID:    req.UserID,
	}

	return pointer, nil
}

//getConversations returns some particular number of conversations after a given point
func (l *latestReader) getConversations(p *model.ReadPointer, req *dto.LatestReadReq) (
	[]*dto.ReadMessage,
	error,
) {
	var conversationID string

	if p.ConversationID != "" {
		conversationID = p.ConversationID
	}

	if req.ConversationID != "" {
		conversationID = req.ConversationID
	}

	var conversationIDPointer *string
	if conversationID != "" {
		conversationIDPointer = &conversationID
	}

	conversations, err := l.conversations.GetByUniqueTagFromID(
		conversationIDPointer,
		p.UniqueTag,
		0,
		int64(l.listLimit),
	)

	if err != nil {
		return nil, fmt.Errorf(
			"could not get conversation by id and uniquetag err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	readMessages := []*dto.ReadMessage{}
	for _, conversation := range conversations {
		readMessage := dto.ReadMessage{}
		readMessage.FromModelConversation(conversation)
		readMessages = append(readMessages, &readMessage)
	}

	return readMessages, nil
}

//updatePointer updates a pointer that have been seen so far
func (l *latestReader) updatePointer(pointer *model.ReadPointer) error {
	return l.readPointers.Save(pointer.ConversationID, pointer.UniqueTag, pointer.UserID)
}

func (l *latestReader) checkConnection(req *dto.LatestReadReq) error {
	ok, err := l.connectionChecker.Check(req.UserID, req.FriendID)
	if err != nil {
		return fmt.Errorf(
			"Could not check connection error=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"Unable to check friendship status", false}},
		)
	}

	if ok {
		return nil
	}

	return &errors.Invalid{errors.Base{"You are not friends", false}}

}

//LatestRead reads the latest read messages
func (l *latestReader) LatestRead(req *dto.LatestReadReq) ([]*dto.ReadMessage, error) {
	if err := l.checkConnection(req); err != nil {
		return nil, err
	}

	pointer, err := l.getPointer(req)
	if err != nil {
		return nil, err
	}

	messages, err := l.getConversations(pointer, req)
	if err != nil {
		return nil, err
	}

	if len(messages) == 0 {
		return messages, nil
	}

	return messages, nil
}

//LatestReaderParams lists parameters for NewLatestReader
type LatestReaderParams struct {
	dig.In
	Conversations     storeconversation.Conversations
	ReadPointers      storeconversation.ReadPointers
	ConnectionChecker connection.Checker
	Cfg               cfg.Limit
}

//NewLatestReader provides a LatestReader
func NewLatestReader(params LatestReaderParams) LatestReader {
	return &latestReader{
		conversations:     params.Conversations,
		readPointers:      params.ReadPointers,
		connectionChecker: params.ConnectionChecker,
		listLimit:         params.Cfg.ChatList,
	}
}
