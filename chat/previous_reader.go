package chat

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	"go.uber.org/dig"
)

//PreviousReader is an interface that defines previous message requests
type PreviousReader interface {
	PreviousRead(*dto.PreviousReadReq) ([]*dto.ReadMessage, error)
}

type previousReader struct {
	conversations     storeconversation.Conversations
	readPointers      storeconversation.ReadPointers
	connectionChecker connection.Checker
	listLimit         int
}

func (p *previousReader) getPointer(req *dto.PreviousReadReq) (
	*model.ReadPointer,
	error,
) {

	uniqueTag := tag.Unique(req.UserID, req.FriendID)
	pointer, err := p.readPointers.GetByUserIDAndTag(req.UserID, uniqueTag)
	if err != nil {
		return nil, fmt.Errorf(
			"could not get read pointer, err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	if pointer != nil {
		return pointer, nil
	}

	pointer = &model.ReadPointer{
		UniqueTag: uniqueTag,
		UserID:    req.UserID,
	}

	return pointer, nil
}

func (p *previousReader) getConversations(pointer *model.ReadPointer, req *dto.PreviousReadReq) (
	[]*dto.ReadMessage,
	error,
) {
	var conversationID string
	if pointer.ConversationID != "" {
		conversationID = pointer.ConversationID
	}

	if req.ConversationID != "" && req.ConversationID <= conversationID {
		conversationID = req.ConversationID
	}

	var conversationIDPointer *string
	if conversationID != "" {
		conversationIDPointer = &conversationID
	}

	conversations, err := p.conversations.GetByUniqueTagFromIDReverse(
		conversationIDPointer,
		pointer.UniqueTag,
		0,
		int64(p.listLimit),
	)

	if err != nil {
		return nil, fmt.Errorf(
			"could not get conversation by id and uniquetag err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	readMessages := []*dto.ReadMessage{}
	for _, conversation := range conversations {
		readMessage := dto.ReadMessage{}
		readMessage.FromModelConversation(conversation)
		readMessages = append(readMessages, &readMessage)
	}

	return readMessages, nil
}

func (p *previousReader) checkConnection(req *dto.PreviousReadReq) error {
	ok, err := p.connectionChecker.Check(req.UserID, req.FriendID)
	if err != nil {
		return fmt.Errorf(
			"Could not check connection err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"Unable to check friendship status", false}},
		)
	}

	if ok {
		return nil
	}

	return &errors.Invalid{errors.Base{"You are not friends", false}}

}

func (p *previousReader) PreviousRead(req *dto.PreviousReadReq) ([]*dto.ReadMessage, error) {
	if err := p.checkConnection(req); err != nil {
		return nil, err
	}

	pointer, err := p.getPointer(req)
	if err != nil {
		return nil, err
	}

	messages, err := p.getConversations(pointer, req)
	if err != nil {
		return nil, err
	}

	return messages, nil
}

//PreviousReaderParams lists NewPreviousReader paramters
type PreviousReaderParams struct {
	dig.In
	Conversations     storeconversation.Conversations
	ReadPointers      storeconversation.ReadPointers
	ConnectionChecker connection.Checker
	Cfg               cfg.Limit
}

//NewPreviousReader provides PreviousReader
func NewPreviousReader(params PreviousReaderParams) PreviousReader {
	return &previousReader{
		conversations:     params.Conversations,
		readPointers:      params.ReadPointers,
		connectionChecker: params.ConnectionChecker,
		listLimit:         params.Cfg.ChatList,
	}
}
