package chat

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/pkg/tag"
	storeconversation "gitlab.com/Aubichol/hrishi-backend/store/conversation"
	"go.uber.org/dig"
)

//LatestUpdater holds database operations related to latest chats interface
type LatestUpdater interface {
	LatestUpdate(*dto.LatestReadReq) (*dto.BaseResponse, error)
}

//latestReader holds database operations related to latest chats handler
type latestUpdater struct {
	conversations     storeconversation.Conversations
	readPointers      storeconversation.ReadPointers
	connectionChecker connection.Checker
	listLimit         int
}

func (l *latestUpdater) getPointer(req *dto.LatestReadReq) (
	*model.ReadPointer,
	error,
) {

	uniqueTag := tag.Unique(req.UserID, req.FriendID)
	pointer, err := l.readPointers.GetByUserIDAndTag(req.UserID, uniqueTag)
	if err != nil {
		return nil, fmt.Errorf(
			"could not get read pointer, err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	if pointer != nil {
		return pointer, nil
	}

	pointer = &model.ReadPointer{
		UniqueTag: uniqueTag,
		UserID:    req.UserID,
	}

	return pointer, nil
}

func (l *latestUpdater) getConversations(p *model.ReadPointer, req *dto.LatestReadReq) (
	[]*dto.ReadMessage,
	error,
) {
	var conversationID string
	if p.ConversationID != "" {
		conversationID = p.ConversationID
	}

	if req.ConversationID != "" && req.ConversationID < conversationID {
		conversationID = req.ConversationID
	}

	var conversationIDPointer *string
	if conversationID != "" {
		conversationIDPointer = &conversationID
	}

	conversations, err := l.conversations.GetByUniqueTagFromID(
		conversationIDPointer,
		p.UniqueTag,
		0,
		int64(l.listLimit),
	)

	if err != nil {
		return nil, fmt.Errorf(
			"could not get conversation by id and uniquetag err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to provide chat list", false}},
		)
	}

	readMessages := []*dto.ReadMessage{}
	for _, conversation := range conversations {
		readMessage := dto.ReadMessage{}
		readMessage.FromModelConversation(conversation)
		readMessages = append(readMessages, &readMessage)
	}

	return readMessages, nil
}

func (l *latestUpdater) updatePointer(pointer *model.ReadPointer) error {
	return l.readPointers.Save(pointer.ConversationID, pointer.UniqueTag, pointer.UserID)
}

func (l *latestUpdater) checkConnection(req *dto.LatestReadReq) error {
	ok, err := l.connectionChecker.Check(req.UserID, req.FriendID)
	if err != nil {
		return fmt.Errorf(
			"Could not check connection error=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"Unable to check friendship status", false}},
		)
	}

	if ok {
		return nil
	}

	return &errors.Invalid{errors.Base{"You are not friends", false}}

}

//LatestRead reads the latest read messages
func (l *latestUpdater) LatestUpdate(req *dto.LatestReadReq) (*dto.BaseResponse, error) {
	if err := l.checkConnection(req); err != nil {
		return nil, err
	}

	pointer, err := l.getPointer(req)
	if err != nil {
		return nil, err
	}

	var resp dto.BaseResponse
	resp.Message = "Pointer has been update"
	resp.OK = true

	if pointer.ConversationID > req.ConversationID {
		return &resp, nil
	}

	pointer.ConversationID = req.ConversationID

	if err = l.updatePointer(pointer); err != nil {
		return nil, fmt.Errorf(
			"unable to update read pointer, err=%s:%w",
			err.Error(),
			&errors.Unknown{errors.Base{"unable to update chat header", false}},
		)
	}

	return &resp, nil
}

//LatestUpdaterParams lists parameters for NewLatestReader
type LatestUpdaterParams struct {
	dig.In
	Conversations     storeconversation.Conversations
	ReadPointers      storeconversation.ReadPointers
	ConnectionChecker connection.Checker
	Cfg               cfg.Limit
}

//NewLatestUpdater provides a LatestReader
func NewLatestUpdater(params LatestUpdaterParams) LatestUpdater {
	return &latestUpdater{
		conversations:     params.Conversations,
		readPointers:      params.ReadPointers,
		connectionChecker: params.ConnectionChecker,
		listLimit:         params.Cfg.ChatList,
	}
}
