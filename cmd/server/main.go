package main

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/bootstrap/server"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/container"
)

func main() {

	if err := server.Viper(); err != nil {
		logrus.Fatal(err)
	}

	c := container.New()
	server.Logrus()
	//Cfg provides configuration related providers to the container
	server.Cfg(c)
	//Mongo provides mongo client to the container
	server.Mongo(c)
	//MongoCollection provides constructor for all the collections of this project
	server.MongoCollections(c)
	//Redis Provides a constructor for creating redis client
	server.Redis(c)

	server.Store(c)
	server.Cache(c)

	server.User(c)
	server.Connection(c)
	server.Chat(c)

	server.Middleware(c)
	server.Route(c)
	server.Status(c)
	server.Token(c)
	server.Comment(c)
	server.Like(c)
	server.Handler(c)

	server.WS(c)
	server.Event(c)

	server.Validator(c)

	c.Resolve(
		func(cfg cfg.Server, handler http.Handler) {
			logrus.Info("Starting server at port ", cfg.Port)
			http.ListenAndServe(":"+cfg.Port, handler)
		})
}
