package status

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"gitlab.com/Aubichol/hrishi-backend/store/status"
	"go.uber.org/dig"
)

//NewsFeedReader provides an interface for reading statuses
type NewsFeedReader interface {
	NewsFeedRead(*dto.NewsFeedReq, int64, int64) (*dto.NewsFeedResp, error)
}

//newsNewsFeedReader implements Reader interface
type newsFeedReader struct {
	statuses status.Status
}

func (read *newsFeedReader) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (read *newsFeedReader) askStore(
	UserID string,
	skip int64,
	limit int64,
) (statuses []*model.Status, err error) {
	statuses, err = read.statuses.FindByUser(
		UserID,
		skip,
		limit,
	)
	return
}

func (read *newsFeedReader) prepareResponse(
	statuses []*model.Status,
) (
	resp dto.NewsFeedResp) {
	resp.FromModel(statuses)
	return
}

func (read *newsFeedReader) NewsFeedRead(
	statusByUserReq *dto.NewsFeedReq,
	skip int64,
	limit int64,
) (*dto.NewsFeedResp, error) {

	statuses, err := read.askStore(
		statusByUserReq.UserID,
		skip,
		limit,
	)

	if err != nil {
		logrus.Error("Could not find status by user error : ", err)
		return nil, read.giveError()
	}

	var resp dto.NewsFeedResp
	resp = read.prepareResponse(statuses)
	return &resp, nil
}

//NewNewsFeedReaderParams lists params for the NewNewsFeedReader
type NewNewsFeedReaderParams struct {
	dig.In
	Status status.Status
}

//NewNewsFeedReader provides Reader
func NewNewsFeedReader(
	params NewNewsFeedReaderParams,
) NewsFeedReader {
	return &newsFeedReader{
		statuses: params.Status,
	}
}
