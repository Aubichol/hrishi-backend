package status

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	storestatus "gitlab.com/Aubichol/hrishi-backend/store/status"
	"go.uber.org/dig"
)

//ByUserReader provides an interface for reading statuses
type ByUserReader interface {
	Read(*dto.ReadByUserReq, int64, int64) (*dto.ReadByUserResp, error)
}

//statusByUserReader implements Reader interface
type statusByUserReader struct {
	statuses storestatus.Status
}

func (read *statusByUserReader) giveError() (err error) {
	err = &errors.Unknown{
		errors.Base{
			"Invalid request", false,
		},
	}
	return
}

func (read *statusByUserReader) askStore(
	UserID string,
	skip int64,
	limit int64,
) (statuses []*model.Status, err error) {
	statuses, err = read.statuses.FindByUser(
		UserID,
		skip,
		limit,
	)
	return
}

func (read *statusByUserReader) processResponse(
	statuses []*model.Status,
) (
	resp dto.ReadByUserResp,
) {
	resp.FromModel(statuses)

	return
}

func (read *statusByUserReader) Read(
	statusByUserReq *dto.ReadByUserReq,
	skip int64,
	limit int64,
) (*dto.ReadByUserResp, error) {
	statuses, err := read.askStore(
		statusByUserReq.UserID,
		skip,
		limit,
	)
	if err != nil {
		logrus.Error("Could not find status by user. Error : ", err)
		return nil, read.giveError()
	}
	var resp dto.ReadByUserResp
	resp = read.processResponse(statuses)
	return &resp, nil
}

//NewByUserReaderParams lists params for the NewReader
type NewByUserReaderParams struct {
	dig.In
	Status storestatus.Status
}

//NewByUserReader provides Reader
func NewByUserReader(params NewByUserReaderParams) ByUserReader {
	return &statusByUserReader{
		statuses: params.Status,
	}
}
