package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Update provides dto for user status update
type Update struct {
	Status   string `json:"status"`
	UserID   string `json:"user_id"`
	StatusID string `json:"status_id"`
}

func (s *Update) giveError() (err error) {
	err = &errors.Invalid{
		errors.Base{
			"invalid status update data", false,
		},
	}
	return
}

//Validate validates status update data
func (s *Update) Validate(validate *validator.Validate) error {
	if err := validate.Struct(s); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			s.giveError(),
		)
	}
	return nil
}

//FromReader decodes status update data from request
func (s *Update) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(s)
	if err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				Base: errors.Base{
					"invalid status update data", false,
				},
			},
		)
	}

	return nil
}
