package dto

//NewsFeedReq helps gather the data for reading the timeline of a by user
type NewsFeedReq struct {
	UserID string
}
