package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//ReadResp holds the response data for reading status
type ReadResp struct {
	Status string `json:"status"`
	Sender string `json:"sender"`
}

//FromModel converts the model data to response data
func (r *ReadResp) FromModel(status *model.Status) {
	r.Status = status.Status
	r.Sender = status.UserID
}
