package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gopkg.in/go-playground/validator.v9"
)

// Status provides dto for user status
type Status struct {
	Status string `json:"status"`
	UserID string `json:"user_id"`
}

//Validate validates status data
func (s *Status) Validate(validate *validator.Validate) error {
	if err := validate.Struct(s); err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{"invalid data", false},
			},
		)
	}
	return nil
}

//FromReader reads json data from request
func (s *Status) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(s)
	if err != nil {
		return fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				Base: errors.Base{"invalid status data", false},
			},
		)
	}

	return nil
}
