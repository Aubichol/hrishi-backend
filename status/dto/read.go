package dto

//ReadReq gather data for reading a specific status
type ReadReq struct {
	UserID   string
	StatusID string
}
