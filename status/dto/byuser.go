package dto

//ReadByUserReq helps gather the data for reading status by user
type ReadByUserReq struct {
	UserID string
}
