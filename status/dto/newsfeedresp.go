package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//NewsFeedResp holds the response data for reading status
type NewsFeedResp struct {
	Statuses []string `json:"status"`
	Senders  []string `json:"sender"`
}

//FromModel converts the model data to response data
func (r *NewsFeedResp) FromModel(status []*model.Status) {
	for _, val := range status {
		r.Statuses = append(r.Statuses, val.Status)
		r.Senders = append(r.Senders, val.UserID)
	}

}
