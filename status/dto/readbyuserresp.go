package dto

import "gitlab.com/Aubichol/hrishi-backend/model"

//StatusResp gives response of one status
type StatusResp struct {
	Status string `json:"status"`
	ID     string `json:"status_id"`
	Time   string `json:"status_time"`
}

//ReadByUserResp holds the response data for reading status
type ReadByUserResp struct {
	Statuses []StatusResp `json:"statuses"`
	Sender   string       `json:"sender"`
}

//FromModel converts the model data to response data
func (r *ReadByUserResp) FromModel(status []*model.Status) {
	var oneStatus StatusResp

	for _, val := range status {
		oneStatus.Status = val.Status
		oneStatus.Time = val.CreatedAt.String()
		oneStatus.ID = val.ID
		r.Statuses = append(r.Statuses, oneStatus)
		r.Sender = val.UserID
	}
	//	r.Statuses = status.Statuses
	//r.Sender = status.UserID
}
