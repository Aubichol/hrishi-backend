package comment

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/comment"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

type readByStatusHandler struct {
	byStatusReader comment.ByStatusReader
}

//querySkip skips number of comments for a request
func (read *readByStatusHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{errors.Base{"Invalid skip", false}},
		)
		return
	}
	return
}

//queryLimit limits number of comments per query
func (read *readByStatusHandler) queryLimit(
	r *http.Request,
) (limit int, err error) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{errors.Base{"Invalid limit", false}})
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{errors.Base{"Limit is too small", false}}
		return
	}

	if limit > 50 {
		err = &errors.Invalid{errors.Base{"Limit is too big", false}}
		return
	}

	return
}

func (read *readByStatusHandler) decodeURL(
	r *http.Request,
) (statusID string) {
	// Read status from database using status id and user id
	statusID = chi.URLParam(r, "id")
	return
}

func (read *readByStatusHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readByStatusHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ReadByStatusResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readByStatusHandler) askController(
	req *dto.ReadByStatusReq,
	skip int,
	limit int,
) (resp *dto.ReadByStatusResp, err error) {
	resp, err = read.byStatusReader.Read(
		req,
		int64(skip),
		int64(limit),
	)
	return
}

func (read *readByStatusHandler) handleRead(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.ReadByStatusReq{}

	req.StatusID = read.decodeURL(r)

	skip, err := read.querySkip(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	limit, err := read.queryLimit(r)

	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	resp, err := read.askController(
		&req,
		skip,
		limit,
	)

	if err != nil {
		message := "Comment get by status error: "
		read.handleError(w, err, message)
		return
	}
	// Serve a response to the client
	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Handler
func (read *readByStatusHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleRead(w, r)
}

//ReadByStatusRouteParams lists all the parameters for NewCommentByStatusRoute
type ReadByStatusRouteParams struct {
	dig.In
	CommentByStatusReader comment.ByStatusReader
	Middleware            *middleware.Auth
}

//ByStatusRoute provides a route to get comments by status id
func ByStatusRoute(
	params ReadByStatusRouteParams,
) *routeutils.Route {

	handler := readByStatusHandler{
		byStatusReader: params.CommentByStatusReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.GetCommentByStatus,
		Handler: params.Middleware.Middleware(&handler),
	}
}
