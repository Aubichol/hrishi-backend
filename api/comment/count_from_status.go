package comment

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/comment"
	"gitlab.com/Aubichol/hrishi-backend/comment/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

type countByStatusHandler struct {
	countByStatusReader comment.CountByStatusReader
}

//querySkip skips number of users for a request
func (read *countByStatusHandler) querySkip(
	r *http.Request,
) (
	skip int,
	err error,
) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{errors.Base{"Invalid skip", false}},
		)
		return
	}
	return
}

//queryLimit limits number of users per query
func (read *countByStatusHandler) queryLimit(
	r *http.Request,
) (
	limit int,
	err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{errors.Base{
				"Invalid limit",
				false,
			},
			},
		)
		return
	}

	if limit < 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small",
				false,
			},
		}
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big",
				false,
			},
		}
		return
	}

	return
}

func (read *countByStatusHandler) decodeURL(
	r *http.Request,
) (statusID string) {
	statusID = chi.URLParam(r, "id")
	return
}

func (read *countByStatusHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *countByStatusHandler) askController(
	message *dto.CountByStatusReq,
) (resp *dto.CountResp, err error) {
	resp, err = read.countByStatusReader.Count(message)
	return
}

func (read *countByStatusHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CountResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *countByStatusHandler) handleCountComment(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.CountByStatusReq{}

	req.StatusID = read.decodeURL(r)
	// Read status from database using status id and user id
	// TO-DO: skip and limit may not be needed here.

	resp, err := read.askController(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}

	// Serve a response to the client
	read.responseSuccess(w, resp)

}

//ServeHTTP implements http.Handler interface
func (read *countByStatusHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleCountComment(w, r)
}

/*CountByStatusParams lists all the parameters
for CountByStatusRoute */
type CountByStatusParams struct {
	dig.In
	CountByStatus comment.CountByStatusReader
	Middleware    *middleware.Auth
}

/*CountByStatusRoute provides a route to count
messages from status*/
func CountByStatusRoute(
	params CountByStatusParams,
) *routeutils.Route {

	handler := countByStatusHandler{
		countByStatusReader: params.CountByStatus,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.CountCommentByStatus,
		Handler: params.Middleware.Middleware(&handler),
	}
}
