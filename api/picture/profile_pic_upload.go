package picture

import (
	"fmt"
	"io"
	"net/http"
	"os"
	"path/filepath"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"go.uber.org/dig"
)

//profilePicUploadHandler holds the handler that uploads profile picture
type profilePicUploadHandler struct {
	cfg cfg.File
}

func (ph *profilePicUploadHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (ph *profilePicUploadHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (ph *profilePicUploadHandler) clearPreImages(
	userID string,
) error {
	matches, err := filepath.Glob(
		fmt.Sprintf("%s/%s.*", ph.cfg.Loc, userID),
	)
	if err != nil {
		logrus.Error(err)
		return err
	}

	for _, loc := range matches {
		if err := os.Remove(loc); err != nil {
			logrus.Error(
				"Could not remove previous image loc=",
				loc,
				", err=",
				err,
			)
			return err
		}
	}

	return nil
}

//ServerHTTP implements http.Handler interface
func (ph *profilePicUploadHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	userID := ph.decodeContext(r)
	if err := ph.clearPreImages(userID); err != nil {
		message := "Unable to clear pre-image"
		ph.handleError(w, err, message)
		return
	}

	r.ParseMultipartForm(ph.cfg.MaxSize)
	fromF, header, err := r.FormFile("file")
	if err != nil {
		message := ""
		ph.handleError(w, err, message)
		return
	}
	defer fromF.Close()

	ext := filepath.Ext(header.Filename)
	toF, err := os.OpenFile(fmt.Sprintf(
		"%s/%s%s",
		ph.cfg.Loc, userID, ext,
	), os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		message := ""
		ph.handleError(w, err, message)
		return
	}
	defer toF.Close()

	_, err = io.Copy(toF, fromF)
	if err != nil {
		message := ""
		ph.handleError(w, err, message)
		return
	}

	routeutils.ServeResponse(
		w,
		http.StatusOK,
		map[string]string{
			"message": "uploaded",
		},
	)
}

//ProfilePicUploadRoute provides a route that uploads
// profile picture
func ProfilePicUploadRoute(params struct {
	dig.In

	Middleware *middleware.Auth
	Cfg        cfg.File
}) *routeutils.Route {
	handler := profilePicUploadHandler{
		cfg: params.Cfg,
	}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.ProfilePictureUpload,
		Handler: params.Middleware.Middleware(&handler),
	}
}
