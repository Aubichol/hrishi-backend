package picture

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
	"go.uber.org/dig"
)

//profilePicSetHandler holds the handler for profile picture setter
type profilePicSetHandler struct {
	user user.ProfilePicSaver
}

func (ph *profilePicSetHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (ph *profilePicSetHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (ph *profilePicSetHandler) decodeBody(body io.ReadCloser) (
	data dto.ProfilePicSaver,
	err error,
) {
	err = data.FromReader(body)
	return
}

func (ph *profilePicSetHandler) askController(
	userID string,
	data dto.ProfilePicSaver,
) (resp *dto.BaseResponse, err error) {
	resp, err = ph.user.SaveProfilePic(userID, data)
	return
}

func (ph *profilePicSetHandler) responseSuccess(
	w http.ResponseWriter,
	response *dto.BaseResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, response)
}

//ServeHTTP implements the http.Handler interface for
//profilePicSetHandler
func (ph *profilePicSetHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()
	userID := ph.decodeContext(r)

	data := dto.ProfilePicSaver{}
	var err error
	data, err = ph.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode err: "
		ph.handleError(w, err, message)
		return
	}
	response, err := ph.askController(userID, data)
	if err != nil {
		message := "Could not update profile pic err: "
		ph.handleError(w, err, message)
		return
	}
	ph.responseSuccess(w, response)
}

//ProfilePicSetParams lists the dependencies of ProfilePicSetRoute
type ProfilePicSetParams struct {
	dig.In
	ProfilePicSaver user.ProfilePicSaver
	Middleware      *middleware.Auth
}

//ProfilePicSetRoute provides a route that sets a new profile picture
func ProfilePicSetRoute(
	params ProfilePicSetParams,
) *routeutils.Route {
	handler := profilePicSetHandler{
		user: params.ProfilePicSaver,
	}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.ProfilePictureSet,
		Handler: params.Middleware.Middleware(&handler),
	}
}
