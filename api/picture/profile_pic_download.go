package picture

import (
	"fmt"
	"net/http"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

//profilePicDownloadHandler holds handler for profile picture download
type profilePicDownloadHandler struct {
	cfg cfg.File
}

func (ph *profilePicDownloadHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (ph *profilePicDownloadHandler) decodeURL(
	r *http.Request,
) (ID string) {
	ID = chi.URLParam(r, "id")
	return
}

//ServeHTTP implements http.Handler
func (ph *profilePicDownloadHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	id := ph.decodeURL(r)
	matches, err := filepath.Glob(fmt.Sprintf("%s/%s.*", ph.cfg.Loc, id))
	if err != nil {
		ph.handleError(w, err)
		return
	}
	if len(matches) == 0 {
		routeutils.ServeError(w, &errors.Invalid{
			Base: errors.Base{
				"Image not found", false,
			},
		})
		return
	}

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "*")
	w.Header().Set("Cache-Control", "no-store")

	http.ServeFile(w, r, matches[0])
}

//ProfilePicDownloadParams gives a list of parameters to ProfilePicDownloadRoute
type ProfilePicDownloadParams struct {
	dig.In
	Cfg        cfg.File
	Middleware *middleware.Auth
}

//ProfilePicDownloadRoute provides a route that downloads profile picture
func ProfilePicDownloadRoute(
	params ProfilePicDownloadParams,
) *routeutils.Route {
	handler := profilePicDownloadHandler{
		cfg: params.Cfg,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: "/api/v1/profilePics/{id}",
		Handler: params.Middleware.Middleware(&handler),
	}
}
