package picture

import (
	"fmt"
	"image/jpeg"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"

	"github.com/disintegration/imageorient"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storepicture "gitlab.com/Aubichol/hrishi-backend/store/picture"
	"go.uber.org/dig"
)

//picUploadHandler holds handler for uploading picture
type picUploadHandler struct {
	cfg   cfg.File
	store storepicture.Pictures
}

//GetFileContentType work TO-DO
func GetFileContentType(out *os.File) (string, error) {

	// Only the first 512 bytes are used to sniff the content type.
	buffer := make([]byte, 512)

	_, err := out.Read(buffer)
	if err != nil {
		return "", err
	}

	// Use the net/http package's handy DectectContentType function.
	//Always returns a valid content-type by returning
	//"application/octet-stream" if no others seemed to match.
	contentType := http.DetectContentType(buffer)

	return contentType, nil
}

func (ph *picUploadHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (ph *picUploadHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

//ServeHTTP implements http.Handler
func (ph *picUploadHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {

	r.ParseMultipartForm(ph.cfg.MaxSize)
	fromFile, header, err := r.FormFile("file")
	if err != nil {
		ph.handleError(w, err)
		return
	}
	defer fromFile.Close()

	userID := ph.decodeContext(r)
	picID := uuid.New().String() +
		filepath.Ext(header.Filename)

	toFile, err := os.OpenFile(fmt.Sprintf(
		"%s/%s",
		ph.cfg.Loc, picID,
	), os.O_WRONLY|os.O_CREATE, 0666)

	if err != nil {
		ph.handleError(w, err)
		return
	}
	defer toFile.Close()

	if err := ph.store.Save(&model.Picture{
		UserID:    userID,
		PictureID: picID,
	}); err != nil {
		ph.handleError(w, err)
		return
	}

	_, err = io.Copy(toFile, fromFile)

	contentType, err := GetFileContentType(toFile)
	if err != nil {
		ph.handleError(w, err)
		return
	}

	if contentType == "image/jpeg" {
		img, _, err := imageorient.Decode(toFile)

		if err != nil {
			log.Fatalf("imageorient.Decode failed: %v", err)
		}

		err = jpeg.Encode(toFile, img, nil)
		if err != nil {
			log.Fatalf("jpeg.Encode failed: %v", err)
		}
	}

	if err != nil {
		ph.handleError(w, err)
		return
	}

	routeutils.ServeResponse(
		w,
		http.StatusOK,
		map[string]string{
			"message":    "uploaded",
			"picture_id": picID,
		},
	)
}

//UploadRouteParams lists parameters for PicUploadRoute
type UploadRouteParams struct {
	dig.In

	Middleware *middleware.Auth
	Cfg        cfg.File
	Store      storepicture.Pictures
}

//UploadRoute provides route for picture upload
func UploadRoute(params UploadRouteParams) *routeutils.Route {
	handler := picUploadHandler{
		cfg:   params.Cfg,
		store: params.Store,
	}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.PictureUpload,
		Handler: params.Middleware.Middleware(&handler),
	}
}
