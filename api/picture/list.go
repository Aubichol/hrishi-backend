package picture

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	storepicture "gitlab.com/Aubichol/hrishi-backend/store/picture"
	"go.uber.org/dig"
)

//Data holds picture response data, this will moved soon
type Data struct {
	URL string `json:"url"`
}

//pictureList holds the handler for listing pictures
type pictureList struct {
	cfg   cfg.File
	store storepicture.Pictures
}

//querySkip skips pictures
func (pl *pictureList) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

//queryLimit limits picture retreaval per query
func (pl *pictureList) queryLimit(r *http.Request) (
	limit int, err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small", false,
			},
		}
		return
	}

	return
}

func (pl *pictureList) handleError(
	w http.ResponseWriter, err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (pl *pictureList) decodeURL(
	r *http.Request,
) (picID string) {
	picID = r.URL.Query().Get("id")
	return
}

func (pl *pictureList) responseSuccess(
	w http.ResponseWriter,
	data []Data,
) {
	routeutils.ServeResponse(w, http.StatusOK, data)
}

func (pl *pictureList) askController(
	picID string, skip, limit int64,
) (
	pictures []*model.Picture,
	err error,
) {
	pictures, err = pl.store.GetByUserID(
		picID,
		int64(skip),
		int64(limit),
	)
	return
}

func (pl *pictureList) decodeURLParams(
	r *http.Request,
) (skip, limit int, err error) {
	skip, err = pl.querySkip(r)
	if err != nil {
		return -1, -1, err
	}
	limit, err = pl.queryLimit(r)
	return
}

func (pl *pictureList) prepareResponse(
	pictures []*model.Picture,
) (data []Data) {
	for _, picture := range pictures {
		data = append(data, Data{
			URL: "/api/v1/pictures/" + picture.PictureID,
		})
	}
	return
}

//ServeHTTP implements http.Handler interface
func (pl *pictureList) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	picid := pl.decodeURL(r)

	skip, limit, err := pl.decodeURLParams(r)
	if err != nil {
		pl.handleError(w, err)
		return
	}

	pictures, err := pl.askController(
		picid,
		int64(skip),
		int64(limit),
	)
	if err != nil {
		pl.handleError(w, err)
		return
	}

	//This processing should happen inside the controller
	var data []Data
	data = pl.prepareResponse(pictures)
	pl.responseSuccess(w, data)
}

//ListParams lists all the dependencies for picture list routes
type ListParams struct {
	dig.In

	Middleware *middleware.Auth
	Cfg        cfg.File
	Store      storepicture.Pictures
}

//ListRoute provides route for picture list
func ListRoute(params ListParams) *routeutils.Route {

	handler := pictureList{
		cfg:   params.Cfg,
		store: params.Store,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.PictureList,
		Handler: params.Middleware.Middleware(&handler),
	}
}
