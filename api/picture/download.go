package picture

import (
	"fmt"
	"net/http"
	"path/filepath"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/cfg"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

type downloadHandler struct {
	cfg cfg.File
}

func (ph *downloadHandler) handleError(
	w http.ResponseWriter, err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (ph *downloadHandler) decodeURL(
	r *http.Request,
) (id string) {
	id = chi.URLParam(r, "id")
	return
}

//ServeHTTP implements http.Handler interface
func (ph *downloadHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	id := ph.decodeURL(r)
	matches, err := filepath.Glob(
		fmt.Sprintf("%s/%s", ph.cfg.Loc, id),
	)
	if err != nil {
		ph.handleError(w, err)
		return
	}
	if len(matches) == 0 {
		routeutils.ServeError(w, &errors.Invalid{
			Base: errors.Base{"Image not found", false},
		})
		return
	}
	//TO-DO: This will be removed from here soon
	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
	w.Header().Set("Access-Control-Allow-Headers", "*")

	http.ServeFile(w, r, matches[0])
}

//DownloadRouteParams lists all the paramters for DownloadRoute
type DownloadRouteParams struct {
	dig.In
	Cfg        cfg.File
	Middleware *middleware.AuthMiddlewareURL
}

//DownloadRoute provides route for picture downloading
func DownloadRoute(
	params DownloadRouteParams,
) *routeutils.Route {
	handler := downloadHandler{
		cfg: params.Cfg,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.PictureDownload,
		Handler: params.Middleware.Middleware(&handler),
	}
}
