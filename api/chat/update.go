package chat

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/chat"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

//readHandler holds chat reading handlers
type updateHandler struct {
	latestUpdater chat.LatestUpdater
}

func (update *updateHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return userID
}

func (update *updateHandler) decodeURL(
	r *http.Request,
) (
	FriendID string,
	ConversationID string,
) {
	FriendID = chi.URLParam(r, "id")
	ConversationID = r.URL.Query().Get("conversation_id")

	return FriendID, ConversationID
}

func (update *updateHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (update *updateHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.BaseResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//handleLatest handles latest chats
func (update *updateHandler) handleUpdate(
	w http.ResponseWriter,
	r *http.Request,
) {
	req := dto.LatestReadReq{}
	req.UserID = update.decodeContext(r)

	req.FriendID, req.ConversationID = update.decodeURL(r)

	//TO-DO: What does this update do?
	resp, err := update.latestUpdater.LatestUpdate(&req)

	if err != nil {
		update.handleError(w, err)
		return
	}

	update.responseSuccess(w, resp)

}

//ServeHTTP implements http.Handler interface
func (update *updateHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	kind := r.URL.Query().Get("kind")
	if kind == chatKindLatest {
		update.handleUpdate(w, r)
		return
	}

	logrus.Error("Invalid kind ", kind)
	routeutils.ServeError(
		w,
		&errors.Invalid{errors.Base{"Invalid kind", false}},
	)
}

//UpdateParams lists all the parameters for UpdateRoute
type UpdateParams struct {
	dig.In
	LatestUpdater chat.LatestUpdater
	Middleware    *middleware.Auth
}

//UpdateRoute provides a chat update route
func UpdateRoute(params UpdateParams) *routeutils.Route {
	handler := updateHandler{
		latestUpdater: params.LatestUpdater,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.UpdateChatRoute,
		Handler: params.Middleware.Middleware(&handler),
	}
}
