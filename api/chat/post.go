package chat

import (
	"io"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/chat"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"go.uber.org/dig"
)

//postHandler holds handler for posting chat
type postHandler struct {
	createConversation chat.CreateConversation
}

func (p *postHandler) decodeRequest(
	body io.ReadCloser,
) (
	message dto.Message,
	err error,
) {
	message = dto.Message{}

	if err = message.FromReader(body); err != nil {
		return message, err
	}

	return message, nil
}

func (p *postHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (p *postHandler) decodeURL(
	r *http.Request,
) (
	friendID string,
) {
	friendID = chi.URLParam(r, "id")
	return
}

func (p *postHandler) decodeContext(
	r *http.Request,
) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (p *postHandler) askController(
	message *dto.Message,
	userID string,
	friendID string,
) (
	resp *dto.ChatPost,
	err error,
) {
	resp, err = p.createConversation.Create(
		message,
		userID,
		friendID,
	)
	return
}

func (p *postHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ChatPost,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler
func (p *postHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	message, err := p.decodeRequest(r.Body)
	if err != nil {
		p.handleError(w, err)
		return
	}

	friendID := p.decodeURL(r)
	userID := p.decodeContext(r)

	resp, err := p.askController(
		&message,
		userID,
		friendID,
	)

	if err != nil {
		p.handleError(w, err)
		return
	}
	p.responseSuccess(w, resp)
}

//PostRouteParams lists parameters for PostRoute
type PostRouteParams struct {
	dig.In
	CreateConversation     chat.CreateConversation
	Middleware             *middleware.Auth
	NotificationMiddleware *middleware.MessageNotification
}

//PostRoute provides route for posting chat
func PostRoute(params PostRouteParams) *routeutils.Route {
	handler := postHandler{params.CreateConversation}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.PostChatRoute,
		Handler: params.Middleware.Middleware(
			params.NotificationMiddleware.Middleware(&handler),
		),
	}
}
