package chat

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/chat"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

const (
	chatKindLatest   string = "latest"
	chatKindPrevious        = "previous"
)

//readHandler holds chat reading handlers
type readHandler struct {
	latestReader   chat.LatestReader
	previousReader chat.PreviousReader
}

func (read *readHandler) decodeContext(
	r *http.Request,
) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return userID
}

func (read *readHandler) decodeURL(
	r *http.Request,
) (FriendID, ConversationID string) {
	FriendID = chi.URLParam(r, "id")
	ConversationID = r.URL.Query().Get("conversation_id")

	return FriendID, ConversationID
}

func (read *readHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readHandler) responseSuccess(
	w http.ResponseWriter,
	resp []*dto.ReadMessage,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//handleLatest handles latest chats
func (read *readHandler) handleLatest(
	w http.ResponseWriter,
	r *http.Request,
) {
	req := dto.LatestReadReq{}
	req.UserID = read.decodeContext(r)

	req.FriendID, req.ConversationID = read.decodeURL(r)

	resp, err := read.latestReader.LatestRead(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}

	read.responseSuccess(w, resp)
}

func (read *readHandler) askController(
	req *dto.PreviousReadReq,
) (
	resp []*dto.ReadMessage,
	err error,
) {
	resp, err = read.previousReader.PreviousRead(req)
	return resp, err
}

//handlePrevious handles previous chats
func (read *readHandler) handlePrevious(
	w http.ResponseWriter,
	r *http.Request,
) {
	req := dto.PreviousReadReq{}
	req.UserID = read.decodeContext(r)

	req.FriendID, req.ConversationID = read.decodeURL(r)

	resp, err := read.askController(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}

	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Handler interface
func (read *readHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	kind := r.URL.Query().Get("kind")
	if kind == chatKindLatest {
		read.handleLatest(w, r)
		return
	}

	if kind == chatKindPrevious {
		read.handlePrevious(w, r)
		return
	}

	logrus.Error("Invalid kind ", kind)
	routeutils.ServeError(
		w,
		&errors.Invalid{errors.Base{"Invalid kind", false}},
	)
}

//ReadRouteParams lists all the parameters for ReadRoute
type ReadRouteParams struct {
	dig.In
	LatestReader   chat.LatestReader
	PreviousReader chat.PreviousReader
	Middleware     *middleware.Auth
}

//ReadRoute provides a route for reading chats
func ReadRoute(params ReadRouteParams) *routeutils.Route {
	handler := readHandler{
		latestReader:   params.LatestReader,
		previousReader: params.PreviousReader,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.ReadChatRoute,
		Handler: params.Middleware.Middleware(&handler),
	}
}
