package chat

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/chat"
	"gitlab.com/Aubichol/hrishi-backend/chat/dto"
	"go.uber.org/dig"
)

//readPointerHandler holds chat reading handlers
type readPointerHandler struct {
	pointerReader chat.PointerReader
}

func (read *readPointerHandler) decodeContext(
	r *http.Request,
) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (read *readPointerHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ReadPointer,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readPointerHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readPointerHandler) decodeURL(
	r *http.Request,
) (
	friendID string,
	conversationID string,
) {
	//TO-DO: This can be passed to the request body.
	/*Question: Is there any advantage passing them in the url
	and query? */
	friendID = chi.URLParam(r, "id")
	conversationID = r.URL.Query().Get("conversation_id")
	return
}

func (read *readPointerHandler) askController(
	req *dto.LatestReadReq,
) (
	resp *dto.ReadPointer,
	err error,
) {
	resp, err = read.pointerReader.PointerRead(req)
	return
}

//handleReadPointer handles read pointer
func (read *readPointerHandler) handleReadPointer(
	w http.ResponseWriter,
	r *http.Request,
) {
	req := dto.LatestReadReq{}

	req.UserID = read.decodeContext(r)
	req.FriendID, req.ConversationID = read.decodeURL(r)

	resp, err := read.askController(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}

	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Handler interface
func (read *readPointerHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleReadPointer(w, r)
}

/*ReadPointerRouteParams lists all the parameters for
ReadPointerRoute */
type ReadPointerRouteParams struct {
	dig.In
	PointerReader chat.PointerReader
	Middleware    *middleware.Auth
}

//ReadPointerRoute provides a chat route that reads chat
func ReadPointerRoute(
	params ReadPointerRouteParams,
) *routeutils.Route {
	handler := readPointerHandler{
		pointerReader: params.PointerReader,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.ReadChatPointerRoute,
		Handler: params.Middleware.Middleware(&handler),
	}
}
