package like

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

type countByStatusHandler struct {
	countByStatusReader like.CountByStatusReader
}

//querySkip skips number of users for a request
func (read *countByStatusHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}

	if skip < 0 {
		err = &errors.Invalid{
			errors.Base{
				"Skip is too small", false,
			},
		}
		return
	}
	return
}

//queryLimit limits number of users per query
func (read *countByStatusHandler) queryLimit(
	r *http.Request,
) (
	limit int,
	err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit < 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small", false,
			},
		}
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (read *countByStatusHandler) decodeURL(
	r *http.Request,
) (statusID string) {
	statusID = chi.URLParam(r, "id")
	return
}

func (read *countByStatusHandler) askController(
	req *dto.CountByStatusReq,
) (resp *dto.CountResp, err error) {
	resp, err = read.countByStatusReader.Count(req)
	return resp, err
}

func (read *countByStatusHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *countByStatusHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CountResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *countByStatusHandler) handleCountLike(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.CountByStatusReq{}

	req.StatusID = read.decodeURL(r)

	resp, err := read.askController(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}
	// Serve a response to the client
	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Server interface
func (read *countByStatusHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleCountLike(w, r)
}

//CountLikeByStatusRouteParams lists all the parameters for CountByStatusRoute
type CountLikeByStatusRouteParams struct {
	dig.In
	CountByStatusReader like.CountByStatusReader
	Middleware          *middleware.Auth
}

//CountByStatusRoute provides a route to get like count for a status
func CountByStatusRoute(
	params CountLikeByStatusRouteParams,
) *routeutils.Route {

	handler := countByStatusHandler{
		countByStatusReader: params.CountByStatusReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.CountLikeFromStatus,
		Handler: params.Middleware.Middleware(&handler),
	}
}
