package like

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

//createHandler holds like handler
type createHandler struct {
	create like.Creater
}

func (ch *createHandler) decodeBody(
	body io.ReadCloser,
) (like dto.Like, err error) {
	like = dto.Like{}
	err = like.FromReader(body)
	return
}

func (ch *createHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (ch *createHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (ch *createHandler) askController(
	like *dto.Like,
) (resp *dto.CreateResponse, err error) {
	resp, err = ch.create.Create(like)
	return
}

func (ch *createHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CreateResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (ch *createHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	like := dto.Like{}
	like, err := ch.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode error: "
		ch.handleError(w, err, message)
		return
	}

	like.UserID = ch.decodeContext(r)

	data, err := ch.askController(&like)

	if err != nil {
		message := "Unable to create like for user error: "
		ch.handleError(w, err, message)
		return
	}

	ch.responseSuccess(w, data)
}

//CreateParams provide parameters for like create handler
type CreateParams struct {
	dig.In
	Create     like.Creater
	Middleware *middleware.Auth
}

//CreateRoute provides a route that registers like creation
func CreateRoute(params CreateParams) *routeutils.Route {
	handler := createHandler{params.Create}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.LikeCreate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
