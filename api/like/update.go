package like

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

//updateHandler holds like update handler
type updateHandler struct {
	update like.Updater
}

func (uh *updateHandler) decodeBody(body io.ReadCloser) (
	like dto.Update, err error,
) {
	err = like.FromReader(body)
	return
}

func (uh *updateHandler) handleError(
	w http.ResponseWriter,
	err error, message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (uh *updateHandler) askController(req *dto.Update) (
	resp *dto.BaseResponse, err error,
) {
	resp, err = uh.update.Update(req)
	return
}

func (uh *updateHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (uh *updateHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.BaseResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (uh *updateHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	like := dto.Update{}
	like, err := uh.decodeBody(r.Body)
	if err != nil {
		message := "Unable to decode status error: "
		uh.handleError(w, err, message)
		return
	}

	like.UserID = uh.decodeContext(r)

	data, err := uh.askController(&like)
	if err != nil {
		message := "Unable to update like for user. Error: "
		uh.handleError(w, err, message)
		return
	}
	uh.responseSuccess(w, data)
}

//UpdateParams provide parameters for status update handler
type UpdateParams struct {
	dig.In
	Update     like.Updater
	Middleware *middleware.Auth
}

//UpdateRoute provides a route that updates status
func UpdateRoute(params UpdateParams) *routeutils.Route {
	handler := updateHandler{params.Update}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.LikeUpdate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
