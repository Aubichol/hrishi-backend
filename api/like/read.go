package like

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

type readHandler struct {
	reader like.Reader
}

func (read *readHandler) decodeBody(body io.ReadCloser) (req dto.ReadReq, err error) {
	err = req.FromReader(body)
	return
}

func (read *readHandler) handleError(w http.ResponseWriter, err error, message string) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (read *readHandler) responseSuccess(w http.ResponseWriter, resp *dto.ReadResp) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readHandler) askController(req *dto.ReadReq) (resp *dto.ReadResp, err error) {
	resp, err = read.reader.Read(req)
	return
}

func (read *readHandler) handleRead(w http.ResponseWriter, r *http.Request) {

	//TO-DO: Status id is missing from here
	req, err := read.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode like error: "
		read.handleError(w, err, message)
		return
	}
	// Get user id from context
	// Read like from database using like id and user id
	resp, err := read.askController(&req)
	if err != nil {
		message := "  "
		read.handleError(w, err, message)
		return
	}
	// Serve a response to the client
	read.responseSuccess(w, resp)
}

func (read *readHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()

	read.handleRead(w, r)
}

//ReadRouteParams lists all the parameters for NewReadLikeRoute
type ReadRouteParams struct {
	dig.In
	LikeReader like.Reader
	Middleware *middleware.Auth
}

//ReadRoute provides a route to get like message
func ReadRoute(params ReadRouteParams) *routeutils.Route {

	handler := readHandler{
		reader: params.LikeReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.LikeGet,
		Handler: params.Middleware.Middleware(&handler),
	}
}
