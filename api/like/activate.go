package like

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

type activateHandler struct {
	activate like.Activater
}

func (ah *activateHandler) decodeBody(
	body io.ReadCloser,
) (
	like dto.Activate,
	err error,
) {
	err = like.FromReader(body)
	return
}

func (ah *activateHandler) askController(
	activate *dto.Activate,
) (
	resp *dto.ActivateResponse,
	err error,
) {
	resp, err = ah.activate.Activate(activate)
	return
}

func (ah *activateHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ActivateResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (ah *activateHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (ah *activateHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

//ServeHTTP implements http.Handler function
func (ah *activateHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	r.Body.Close()

	like := dto.Activate{}
	var err error

	//decode like id from body
	like, err = ah.decodeBody(r.Body)

	//take userid from context
	like.UserID = ah.decodeContext(r)
	//response error if body has issues
	if err != nil {
		message := "Like Request Body Had Errors"
		ah.handleError(w, err, message)
	}

	//send it to the controller
	resp, err := ah.askController(&like)
	//check if controller gave any errors
	if err != nil {
		message := "Like Controller Had Errors"
		ah.handleError(w, err, message)
	}

	ah.responseSuccess(w, resp)
	//return successful response
}

//ActivateRouteParams provides parameters to ActivateRoute
type ActivateRouteParams struct {
	dig.In
	Activate   like.Activater
	Middleware *middleware.Auth
}

//ActivateRoute provides route for activating likes
func ActivateRoute(
	params ActivateRouteParams,
) *routeutils.Route {
	handler := activateHandler{params.Activate}

	return &routeutils.Route{
		Method:  http.MethodPost,
		Handler: params.Middleware.Middleware(&handler),
		Pattern: apipattern.LikeActivate,
	}

}
