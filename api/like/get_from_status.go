package like

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/like"
	"gitlab.com/Aubichol/hrishi-backend/like/dto"
	"go.uber.org/dig"
)

type readByStatusHandler struct {
	byStatusReader like.ByStatusReader
}

//querySkip skips number of likes for a request
func (read *readByStatusHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	if skip < 0 {
		err = &errors.Invalid{
			errors.Base{
				"Skip is too small", false,
			},
		}
		return
	}
	return
}

//queryLimit limits number of likes per query
func (read *readByStatusHandler) queryLimit(
	r *http.Request,
) (limit int, err error) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small", false,
			},
		}
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (read *readByStatusHandler) askController(
	req dto.ReadByStatusReq,
	skip int64,
	limit int64,
) (resp *dto.ReadByStatusResp, err error) {
	resp, err = read.byStatusReader.Read(
		&req,
		int64(skip),
		int64(limit),
	)
	return
}

func (read *readByStatusHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readByStatusHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ReadByStatusResp,
) {
	// Serve a response to the client
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readByStatusHandler) decodeURL(
	r *http.Request,
) (statusID string) {
	statusID = chi.URLParam(r, "id")
	return
}

func (read *readByStatusHandler) handleRead(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.ReadByStatusReq{}

	req.StatusID = read.decodeURL(r)

	skip, err := read.querySkip(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	limit, err := read.queryLimit(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	resp, err := read.askController(
		req,
		int64(skip),
		int64(limit),
	)

	if err != nil {
		read.handleError(w, err)
		return
	}

	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Handler interface
func (read *readByStatusHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleRead(w, r)
}

//ReadByStatusRouteParams lists all the parameters for NewByStatusRoute
type ReadByStatusRouteParams struct {
	dig.In
	ByStatusReader like.ByStatusReader
	Middleware     *middleware.Auth
}

//ByStatusRoute provides a route to get likes list from status
func ByStatusRoute(
	params ReadByStatusRouteParams,
) *routeutils.Route {

	handler := readByStatusHandler{
		byStatusReader: params.ByStatusReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.GetLikeFromStatus,
		Handler: params.Middleware.Middleware(&handler),
	}
}
