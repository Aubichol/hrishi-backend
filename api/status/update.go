package status

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/status"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"go.uber.org/dig"
)

//updateHandler holds status update handler
type updateHandler struct {
	update status.Updater
}

func (uh *updateHandler) decodeBody(body io.ReadCloser) (
	status dto.Update,
	err error,
) {
	err = status.FromReader(body)
	return
}

func (uh *updateHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (uh *updateHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (uh *updateHandler) askController(req *dto.Update) (
	resp *dto.UpdateResponse,
	err error,
) {
	resp, err = uh.update.Update(req)
	return
}

func (uh *updateHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.UpdateResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (uh *updateHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	status := dto.Update{}
	var err error
	status, err = uh.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode status error: "
		uh.handleError(w, err, message)
		return
	}
	status.UserID = uh.decodeContext(r)

	data, err := uh.askController(&status)
	if err != nil {
		message := "Unable to update status for user error: "
		uh.handleError(w, err, message)
		return
	}
	uh.responseSuccess(w, data)
}

//UpdateParams provide parameters for status update handler
type UpdateParams struct {
	dig.In
	Update     status.Updater
	Middleware *middleware.Auth
}

//UpdateRoute provides a route that updates status
func UpdateRoute(params UpdateParams) *routeutils.Route {
	handler := updateHandler{params.Update}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.StatusUpdate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
