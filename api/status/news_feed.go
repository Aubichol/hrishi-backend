package status

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/status"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"go.uber.org/dig"
)

type newsFeedHandler struct {
	newsFeedReader status.NewsFeedReader
}

//querySkip skips number of statuses for a news feed
func (nf *newsFeedHandler) querySkip(r *http.Request) (
	skip int, err error,
) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

//queryLimit limits number of news feeds per query
func (nf *newsFeedHandler) queryLimit(r *http.Request) (
	limit int,
	err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (nf *newsFeedHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (nf *newsFeedHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (nf *newsFeedHandler) askController(
	req *dto.NewsFeedReq,
	skip int64,
	limit int64,
) (resp *dto.NewsFeedResp, err error) {
	resp, err = nf.newsFeedReader.NewsFeedRead(
		req,
		int64(skip),
		int64(limit),
	)
	return resp, err
}

func (nf *newsFeedHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.NewsFeedResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (nf *newsFeedHandler) handleNewsFeed(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.NewsFeedReq{}
	req.UserID = nf.decodeContext(r)

	skip, err := nf.querySkip(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	limit, err := nf.queryLimit(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	resp, err := nf.askController(
		&req,
		int64(skip),
		int64(limit),
	)

	if err != nil {
		nf.handleError(w, err)
		return
	}
	// Serve a response to the client
	nf.responseSuccess(w, resp)
}

//ServeHTTP implements ServeHTTP handler
func (nf *newsFeedHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	nf.handleNewsFeed(w, r)
}

//NewsFeedRouteParams lists all the parameters for NewNewsFeedRoute
type NewsFeedRouteParams struct {
	dig.In
	NewsFeedReader status.NewsFeedReader
	Middleware     *middleware.Auth
}

//NewsFeedRoute provides a route to get news feed route
func NewsFeedRoute(params NewsFeedRouteParams) *routeutils.Route {

	handler := newsFeedHandler{
		newsFeedReader: params.NewsFeedReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.NewsFeed,
		Handler: params.Middleware.Middleware(&handler),
	}
}
