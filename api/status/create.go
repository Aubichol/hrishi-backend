package status

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/status"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"go.uber.org/dig"
)

//createHandler holds status create handler
type createHandler struct {
	create status.Creater
}

func (sh *createHandler) decodeBody(body io.ReadCloser) (
	status dto.Status,
	err error,
) {
	err = status.FromReader(body)
	return
}

func (sh *createHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (sh *createHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (sh *createHandler) askController(status *dto.Status) (
	data *dto.CreateResponse,
	err error,
) {
	data, err = sh.create.Create(status)
	return
}

func (sh *createHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.CreateResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (sh *createHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	status := dto.Status{}
	var err error
	status, err = sh.decodeBody(r.Body)
	if err != nil {
		message := "Unable to decode error: "
		sh.handleError(w, err, message)
		return
	}
	//Only logged user can create status
	status.UserID = sh.decodeContext(r)

	data, err := sh.askController(&status)
	if err != nil {
		message := "Unable to create status for user. Error: "
		sh.handleError(w, err, message)
		return
	}
	sh.responseSuccess(w, data)
}

//CreateParams provide parameters for status create handler
type CreateParams struct {
	dig.In
	Create     status.Creater
	Middleware *middleware.Auth
}

//CreateRoute provides a route that create user status
func CreateRoute(params CreateParams) *routeutils.Route {
	handler := createHandler{params.Create}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.StatusCreate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
