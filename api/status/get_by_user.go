package status

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/status"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"go.uber.org/dig"
)

type readByUserHandler struct {
	byUserReader status.ByUserReader
}

//querySkip skips number of statuses for a user in a request
func (read *readByUserHandler) querySkip(r *http.Request) (
	skip int,
	err error,
) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}

	if skip < 0 {
		err = &errors.Invalid{
			errors.Base{
				"Skip is too small", false,
			},
		}
		return
	}
	return
}

//queryLimit limits number of statuses per query
func (read *readByUserHandler) queryLimit(r *http.Request) (limit int, err error) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))

	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small", false,
			},
		}
		return
	}
	return
}

func (read *readByUserHandler) decodeURL(r *http.Request) (
	userID string,
) {
	// Read status from database using user id
	userID = chi.URLParam(r, "id")
	return
}

func (read *readByUserHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readByUserHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.ReadByUserResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readByUserHandler) askController(
	req *dto.ReadByUserReq,
	skip int64,
	limit int64,
) (
	resp *dto.ReadByUserResp,
	err error,
) {
	resp, err = read.byUserReader.Read(req, skip, limit)
	return
}

func (read *readByUserHandler) handleReadStatus(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.ReadByUserReq{}
	req.UserID = read.decodeURL(r)

	skip, err := read.querySkip(r)
	if err != nil {
		read.handleError(w, err)
		return
	}

	limit, err := read.queryLimit(r)
	if err != nil {
		read.handleError(w, err)
		return
	}

	resp, err := read.askController(&req, int64(skip), int64(limit))
	if err != nil {
		read.handleError(w, err)
		return
	}
	// Serve a response to the client
	read.responseSuccess(w, resp)
}

//ServeHTTP implements http.Handler interface
func (read *readByUserHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleReadStatus(w, r)
}

//ReadByUserRouteParams lists all the parameters for
//NewStatusByUserRoute
type ReadByUserRouteParams struct {
	dig.In
	ByUserReader status.ByUserReader
	Middleware   *middleware.Auth
}

//ByUserRoute provides a route to get a list of status by user id
func ByUserRoute(params ReadByUserRouteParams) *routeutils.Route {

	handler := readByUserHandler{
		byUserReader: params.ByUserReader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.StatusByUser,
		Handler: params.Middleware.Middleware(&handler),
	}
}
