package status

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/status"
	"gitlab.com/Aubichol/hrishi-backend/status/dto"
	"go.uber.org/dig"
)

//readHandler provides the handler for reading status
type readHandler struct {
	reader status.Reader
}

func (read *readHandler) decodeURL(r *http.Request) (
	statusID string,
) {
	statusID = chi.URLParam(r, "id")
	return
}

func (read *readHandler) decodeContext(r *http.Request) (
	userID string,
) {
	//Decode userID from context
	userID = r.Context().Value("userID").(string)
	return
}

func (read *readHandler) askController(req *dto.ReadReq) (
	resp *dto.ReadResp,
	err error,
) {
	resp, err = read.reader.Read(req)
	return
}

func (read *readHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error(err)
	routeutils.ServeError(w, err)
}

func (read *readHandler) resonseSuccess(
	w http.ResponseWriter,
	resp *dto.ReadResp,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

func (read *readHandler) handleReadStatus(
	w http.ResponseWriter,
	r *http.Request,
) {

	req := dto.ReadReq{}
	req.StatusID = read.decodeURL(r)
	req.UserID = read.decodeContext(r)

	// Read status from database using status id and user id
	resp, err := read.askController(&req)

	if err != nil {
		read.handleError(w, err)
		return
	}
	// Serve a response to the client
	read.resonseSuccess(w, resp)
}

//ServeHTTP implements http.Handler
func (read *readHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	read.handleReadStatus(w, r)
}

//ReadRouteParams lists all the parameters for NewReadRoute
type ReadRouteParams struct {
	dig.In
	Reader     status.Reader
	Middleware *middleware.Auth
}

//ReadRoute provides a route to get status message
func ReadRoute(params ReadRouteParams) *routeutils.Route {

	handler := readHandler{
		reader: params.Reader,
	}

	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.StatusRead,
		Handler: params.Middleware.Middleware(&handler),
	}
}
