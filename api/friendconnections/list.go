package friendconnections

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.uber.org/dig"
)

type friendListHandler struct {
	activeFriendList  connection.FriendList
	pendingFriendList connection.FriendList
	blockedFriendList connection.FriendList
}

//queryStatus checks which type of status has been requested
func (f *friendListHandler) queryStatus(
	r *http.Request,
) (
	status string,
	err error,
) {
	status = r.URL.Query().Get("status")
	if status == "" {
		err = &errors.Invalid{
			errors.Base{
				"invalid status query", false,
			},
		}
		return
	}

	if status != model.AcceptedFriendStatus.String() &&
		status != model.PendingFriendStatus.String() &&
		status != model.BlockedFriendStatus.String() {
		err = &errors.Invalid{
			errors.Base{
				"invalid status query", false,
			},
		}
	}

	return
}

//querySkip decodes the skip parameter from request
func (f *friendListHandler) querySkip(
	r *http.Request,
) (
	skip int,
	err error,
) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"invalid skip", false,
				},
			},
		)
		return
	}
	return
}

//queryLimit decodes limit parameter from request
func (f *friendListHandler) queryLimit(
	r *http.Request,
) (
	limit int,
	err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too small", false,
			},
		}
		return
	}

	return
}

//ServeHTTP implements http.Handler interface
func (f *friendListHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()
	userID := r.Context().Value("userID").(string)

	status, err := f.queryStatus(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	skip, err := f.querySkip(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	limit, err := f.queryLimit(r)
	if err != nil {
		routeutils.ServeError(w, err)
		return
	}

	var friends []*dto.Friend

	if status == model.AcceptedFriendStatus.String() {
		friends, err = f.activeFriendList.Friends(
			userID, skip, limit,
		)
	}

	if status == model.PendingFriendStatus.String() {
		friends, err = f.pendingFriendList.Friends(
			userID, skip, limit,
		)
	}

	if status == model.BlockedFriendStatus.String() {
		friends, err = f.blockedFriendList.Friends(
			userID, skip, limit,
		)
	}

	if err != nil {
		logrus.Error("Could generate friend list, err : ", err)
		routeutils.ServeError(w, err)
		return
	}

	routeutils.ServeResponse(
		w,
		http.StatusOK,
		friends,
	)
}

//FriendListRouteParams lists dependencies for NewFriendListRoute
type FriendListRouteParams struct {
	dig.In
	Active     connection.FriendList `name:"active"`
	Pending    connection.FriendList `name:"pending"`
	Blocked    connection.FriendList `name:"blocked"`
	Middleware *middleware.Auth
}

//FriendListRoute provides route for list of different kind of friends
func FriendListRoute(
	params FriendListRouteParams,
) *routeutils.Route {
	handler := friendListHandler{
		activeFriendList:  params.Active,
		pendingFriendList: params.Pending,
		blockedFriendList: params.Blocked,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.FriendList,
		Handler: params.Middleware.Middleware(&handler),
	}
}
