package friendconnections

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.uber.org/dig"
)

type openFriendHandler struct {
	requester connection.Requester
}

func (f *openFriendHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (f *openFriendHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.BaseResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

func (f *openFriendHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (f *openFriendHandler) decodeBody(
	body io.ReadCloser,
) (
	req dto.FriendRequest,
	err error,
) {
	err = req.FromReader(body)
	return
}

func (f *openFriendHandler) handleOpen(
	w http.ResponseWriter,
	fromUserID string,
	toUserID string,
) {
	response, err := f.requester.OpenFriendRequest(
		fromUserID, toUserID,
	)
	if err != nil {
		message := "Unable to open friend request, err: "
		f.handleError(w, err, message)
		return
	}

	f.responseSuccess(w, response)
}

func (f *openFriendHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close() // needs to add this to other projects as well

	userID := f.decodeContext(r)
	request := dto.FriendRequest{}
	var err error
	request, err = f.decodeBody(r.Body)

	if err != nil {
		logrus.Error("Unable to decode, err: ", err)
		routeutils.ServeError(
			w,
			&errors.Invalid{
				errors.Base{
					"Invalid data", false,
				},
			},
		)
		return
	}

	if request.Action == model.OpenFriendStatus.String() &&
		request.ID != nil {
		f.handleOpen(w, userID, *request.ID)
		return
	}

	routeutils.ServeResponse(
		w,
		http.StatusOK,
		&errors.Invalid{
			errors.Base{
				"Invalid action", false,
			},
		},
	)
}

//OpenFriendRouteParams lists paramters for NewOpenRequestRoute
type OpenFriendRouteParams struct {
	dig.In
	Requester  connection.Requester
	Middleware *middleware.Auth
}

//OpenRequestRoute provides route for opening a frined request
func OpenRequestRoute(
	params OpenFriendRouteParams,
) *routeutils.Route {
	handler := openFriendHandler{
		requester: params.Requester,
	}
	return &routeutils.Route{
		Method:  http.MethodPost,
		Pattern: apipattern.FriendOpen,
		Handler: params.Middleware.Middleware(&handler),
	}
}
