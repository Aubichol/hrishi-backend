package friendconnections

import (
	"io"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"gitlab.com/Aubichol/hrishi-backend/model"
	"go.uber.org/dig"
)

//updateFriendHandler holds updateFriendHandler
type updateFriendHandler struct {
	rejector   connection.Rejector
	acceptor   connection.Acceptor
	blocker    connection.Blocker
	unblocker  connection.Unblocker
	unfriender connection.Unfriender
}

func (u *updateFriendHandler) decodeURL(r *http.Request) (id string) {
	id = chi.URLParam(r, "id")
	return
}

func (u *updateFriendHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (u *updateFriendHandler) askController(id, userID string) (
	response *dto.BaseResponse,
	err error,
) {
	response, err = u.unfriender.Unfriend(id, userID)
	return
}

func (u *updateFriendHandler) responseSuccess(
	w http.ResponseWriter,
	response *dto.BaseResponse,
) {
	routeutils.ServeResponse(w, http.StatusOK, response)
}

func (u *updateFriendHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (u *updateFriendHandler) decodeBody(body io.ReadCloser) (
	request dto.FriendRequest,
	err error,
) {
	err = request.FromReader(body)
	return
}

//handleUnfriend handles unfriending someone
func (u *updateFriendHandler) handleUnfriend(
	w http.ResponseWriter,
	r *http.Request,
	userID string,
) {
	id := u.decodeURL(r)
	response, err := u.askController(id, userID)
	if err != nil {
		message := "Unable to unfriend friend, err: "
		u.handleError(w, err, message)
		return
	}

	u.responseSuccess(w, response)
}

//handleReject handles rejecting someone
func (u *updateFriendHandler) handleReject(
	w http.ResponseWriter,
	r *http.Request,
	userID string,
) {
	id := u.decodeURL(r)
	response, err := u.rejector.Reject(id, userID)
	if err != nil {
		message := "Unable to reject friend request, err: "
		u.handleError(w, err, message)
		return
	}

	u.responseSuccess(w, response)
}

//handleAccept handles accepting someone
func (u *updateFriendHandler) handleAccept(
	w http.ResponseWriter,
	r *http.Request,
	userID string,
) {
	id := u.decodeURL(r)
	response, err := u.acceptor.Accept(id, userID)
	if err != nil {
		message := "Unable to accept friend request, err: "
		u.handleError(w, err, message)
		return
	}

	u.responseSuccess(w, response)
}

//handleBlock handles blocking someone
func (u *updateFriendHandler) handleBlock(
	w http.ResponseWriter,
	r *http.Request,
	userID string,
) {
	id := u.decodeURL(r)
	response, err := u.blocker.Block(id, userID)
	if err != nil {
		message := "Unable to block friend request, err: "
		u.handleError(w, err, message)
		return
	}

	u.responseSuccess(w, response)
}

//handleUnblock handles unblocking someone
func (u *updateFriendHandler) handleUnblock(
	w http.ResponseWriter,
	r *http.Request,
	userID string,
) {
	id := u.decodeURL(r)
	response, err := u.unblocker.Unblock(id, userID)
	if err != nil {
		message := "Unable to unblock friend request, err: "
		u.handleError(w, err, message)
		return
	}

	u.responseSuccess(w, response)
}

//ServeHTTP implements http.Handler
func (u *updateFriendHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	userID := u.decodeContext(r)
	request := dto.FriendRequest{}
	var err error
	request, err = u.decodeBody(r.Body)
	if err != nil {
		logrus.Error("Unable to decode, err: ", err)
		routeutils.ServeError(
			w,
			&errors.Invalid{
				errors.Base{
					"Invalid data", false,
				},
			},
		)
		return
	}

	if request.Action == model.AcceptedFriendStatus.String() {
		u.handleAccept(w, r, userID)
		return
	}

	if request.Action == model.RejectedFriendStatus.String() {
		u.handleReject(w, r, userID)
		return
	}

	if request.Action == model.BlockedFriendStatus.String() {
		u.handleBlock(w, r, userID)
		return
	}

	if request.Action == model.UnfriendFriendStatus.String() {
		u.handleUnfriend(w, r, userID)
		return
	}

	if request.Action == model.UnblockedFriendStatus.String() {
		u.handleUnblock(w, r, userID)
		return
	}

	routeutils.ServeResponse(
		w,
		http.StatusOK,
		&errors.Invalid{
			errors.Base{
				"Invalid action", false,
			},
		},
	)

}

//UpdateFriendRouteParams lists all the dependencies that NewUpdateFriendRoute needs
type UpdateFriendRouteParams struct {
	dig.In
	Rejector   connection.Rejector
	Acceptor   connection.Acceptor
	Blocker    connection.Blocker
	Unblocker  connection.Unblocker
	Unfriender connection.Unfriender
	Middleware *middleware.Auth
}

//UpdateFriendRoute provides route that updates friendship status
func UpdateFriendRoute(params UpdateFriendRouteParams) *routeutils.Route {
	handler := updateFriendHandler{
		rejector:   params.Rejector,
		acceptor:   params.Acceptor,
		blocker:    params.Blocker,
		unblocker:  params.Unblocker,
		unfriender: params.Unfriender,
	}
	return &routeutils.Route{
		Method:  http.MethodPut,
		Pattern: apipattern.FriendUpdate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
