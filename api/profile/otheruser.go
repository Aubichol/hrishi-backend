package profile

import (
	"net/http"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
	"go.uber.org/dig"
)

//otherUserHandler holds handler for giving profile information
//of other users
type otherUserHandler struct {
	myProfile user.MyProfiler
}

func (m *otherUserHandler) decodeURL(r *http.Request) (
	userID string,
) {
	userID = chi.URLParam(r, "id")
	return
}

func (m *otherUserHandler) askController(userID string) (
	data *dto.Me,
	err error,
) {
	data, err = m.myProfile.Me(userID)
	return
}

func (m *otherUserHandler) handleError(
	w http.ResponseWriter,
	err error,
) {
	logrus.Error("Could not get my profile error: ", err)
	routeutils.ServeError(w, err)
}

func (m *otherUserHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.Me,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (m *otherUserHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	userID := m.decodeURL(r)

	data, err := m.askController(userID)
	if err != nil {
		m.handleError(w, err)
		return
	}

	m.responseSuccess(w, data)
}

//OtherUserParams lists all the dependencies for NewMeRoute
type OtherUserParams struct {
	dig.In
	MyProfile  user.MyProfiler
	Middleware *middleware.Auth
}

//OtherUserRoute provides a route that gets a users
//other profile information
func OtherUserRoute(params OtherUserParams) *routeutils.Route {
	handler := otherUserHandler{
		myProfile: params.MyProfile,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.OtherUser,
		Handler: params.Middleware.Middleware(&handler),
	}
}
