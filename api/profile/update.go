package profile

import (
	"io"
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
	"go.uber.org/dig"
)

//updateHandler holds the handler that updates a profile
type updateHandler struct {
	profileUpdater user.ProfileUpdater
}

func (uh *updateHandler) decodeContext(
	r *http.Request,
) (userID string) {
	userID = r.Context().Value("userID").(string)
	return
}

func (uh *updateHandler) decodeBody(body io.ReadCloser) (
	data dto.MeUpdate,
	err error,
) {
	err = data.FromReader(body)
	return
}

func (uh *updateHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (uh *updateHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.BaseResponse,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		resp,
	)
}

func (uh *updateHandler) askController(
	userID string,
	data *dto.MeUpdate,
) (
	resp *dto.BaseResponse,
	err error,
) {
	resp, err = uh.profileUpdater.ProfileUpdate(userID, data)
	return
}

//ServeHTTP implements http.Handler interface
//for profileUpdatHandler
func (uh *updateHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()

	userID := uh.decodeContext(r)

	data := dto.MeUpdate{}
	data, err := uh.decodeBody(r.Body)

	if err != nil {
		message := "Unable to decode err: "
		uh.handleError(w, err, message)
		return
	}

	response, err := uh.askController(userID, &data)
	if err != nil {
		message := "Could not update profile err: "
		uh.handleError(w, err, message)
		return
	}

	uh.responseSuccess(w, response)
}

//UpdaterParams lists dependencies for NewProfileUpdaterRoute
type UpdaterParams struct {
	dig.In
	ProfileUpdater user.ProfileUpdater
	Middleware     *middleware.Auth
}

//UpdateRoute provides a route that updates a profile
func UpdateRoute(params UpdaterParams) *routeutils.Route {
	handler := updateHandler{
		profileUpdater: params.ProfileUpdater,
	}
	return &routeutils.Route{
		Method:  http.MethodPut,
		Pattern: apipattern.ProfileUpdate,
		Handler: params.Middleware.Middleware(&handler),
	}
}
