package profile

import (
	"net/http"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
	"go.uber.org/dig"
)

//meHandler holds handler for providing personal profile information
type userHandler struct {
	myProfile user.MyProfiler
}

func (m *userHandler) decodeContext(r *http.Request) (
	userID string,
) {
	userID = r.Context().Value("userID").(string)
	return
}

func (m *userHandler) askController(userID string) (
	data *dto.Me,
	err error,
) {
	data, err = m.myProfile.Me(userID)
	return
}

func (m *userHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (m *userHandler) responseSuccess(
	w http.ResponseWriter,
	resp *dto.Me,
) {
	routeutils.ServeResponse(w, http.StatusOK, resp)
}

//ServeHTTP implements http.Handler interface
func (m *userHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	userID := m.decodeContext(r)

	data, err := m.askController(userID)
	if err != nil {
		message := "Could not get my profile err: "
		m.handleError(w, err, message)
		return
	}

	m.responseSuccess(w, data)
}

//UserRouteParams lists all the dependencies for NewMeRoute
type UserRouteParams struct {
	dig.In
	MyProfile  user.MyProfiler
	Middleware *middleware.Auth
}

//UserRoute provides a route that gets a users own profile info
func UserRoute(params UserRouteParams) *routeutils.Route {
	handler := userHandler{
		myProfile: params.MyProfile,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.ProfileUser,
		Handler: params.Middleware.Middleware(&handler),
	}
}
