package user

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

//allPublicHandler holds the handler for all users
type allPublicHandler struct {
	publicUserList connection.PublicUserList
}

//querySkip skips number of public users for a request
func (f *allPublicHandler) querySkip(r *http.Request) (
	skip int, err error,
) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

//queryLimit limits number of public users per query
func (f *allPublicHandler) queryLimit(r *http.Request) (
	limit int, err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"limit is too big", false,
			},
		}
		return
	}

	if limit <= 0 {
		err = &errors.Invalid{
			errors.Base{
				"limit is too small", false,
			},
		}
		return
	}

	return
}

func (f *allPublicHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

func (f *allPublicHandler) askController(
	skip int, limit int,
) (
	users []*dto.PublicUser,
	err error,
) {
	users, err = f.publicUserList.Users(skip, limit)
	return
}

func (f *allPublicHandler) responseSuccess(
	w http.ResponseWriter,
	users []*dto.PublicUser,
) {
	routeutils.ServeResponse(
		w,
		http.StatusOK,
		users,
	)
}

//ServeHTTP implements http.Handler interface
func (f *allPublicHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()
	skip, err := f.querySkip(r)
	if err != nil {
		message := "Unable to get skip: "
		f.handleError(w, err, message)
		return
	}

	limit, err := f.queryLimit(r)
	if err != nil {
		message := "Unable to get limit: "
		f.handleError(w, err, message)
		return
	}

	var users []*dto.PublicUser

	users, err = f.askController(skip, limit)

	if err != nil {
		message := "Could generate user list, err : "
		f.handleError(w, err, message)
		return
	}

	f.responseSuccess(w, users)
}

//PublicListRouteParams lists all the dependencies
//of NewPublicListRoute
type PublicListRouteParams struct {
	dig.In
	User connection.PublicUserList `name:"public"`
}

//PublicListRoute provides routes for all the users
func PublicListRoute(
	params PublicListRouteParams,
) *routeutils.Route {
	handler := allPublicHandler{
		publicUserList: params.User,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.PublicUserList,
		Handler: &handler,
	}
}
