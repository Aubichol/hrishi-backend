package user

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/Aubichol/hrishi-backend/api/middleware"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/apipattern"
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/connection/dto"
	"gitlab.com/Aubichol/hrishi-backend/errors"
	"go.uber.org/dig"
)

//allHandler holds the handler for all users
type allHandler struct {
	activeUserList connection.UserList
}

//querySkip skips number of users for a request
func (f *allHandler) querySkip(
	r *http.Request,
) (skip int, err error) {
	skip, err = strconv.Atoi(r.URL.Query().Get("skip"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid skip", false,
				},
			},
		)
		return
	}
	return
}

//queryLimit limits number of users per query
func (f *allHandler) queryLimit(r *http.Request) (
	limit int, err error,
) {
	limit, err = strconv.Atoi(r.URL.Query().Get("limit"))
	if err != nil {
		err = fmt.Errorf(
			"%s:%w",
			err.Error(),
			&errors.Invalid{
				errors.Base{
					"Invalid limit", false,
				},
			},
		)
		return
	}

	if limit > 50 {
		err = &errors.Invalid{
			errors.Base{
				"Limit is too big", false,
			},
		}
		return
	}

	return
}

func (f *allHandler) handleError(
	w http.ResponseWriter,
	err error,
	message string,
) {
	logrus.Error(message, err)
	routeutils.ServeError(w, err)
}

//ServeHTTP implements http.Handler interface
func (f *allHandler) ServeHTTP(
	w http.ResponseWriter,
	r *http.Request,
) {
	defer r.Body.Close()
	userID := r.Context().Value("userID").(string)

	skip, err := f.querySkip(r)
	if err != nil {
		message := "Unable to extract skip: "
		f.handleError(w, err, message)
		return
	}

	limit, err := f.queryLimit(r)
	if err != nil {
		message := "Unable to extract limit: "
		f.handleError(w, err, message)
		return
	}

	var users []*dto.User

	users, err = f.activeUserList.Users(
		userID, skip, limit,
	)

	if err != nil {
		message := "Could generate friend list, err : "
		f.handleError(w, err, message)
		return
	}

	routeutils.ServeResponse(w, http.StatusOK, users)
}

//ListRouteParams lists all the dependencies of NewUserListRoute
type ListRouteParams struct {
	dig.In
	User       connection.UserList `name:"all"`
	Middleware *middleware.Auth
}

//ListRoute provides routes for all the users
func ListRoute(params ListRouteParams) *routeutils.Route {
	handler := allHandler{
		activeUserList: params.User,
	}
	return &routeutils.Route{
		Method:  http.MethodGet,
		Pattern: apipattern.UserList,
		Handler: params.Middleware.Middleware(&handler),
	}
}
