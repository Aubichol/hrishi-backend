module gitlab.com/Aubichol/hrishi-backend

go 1.13

require (
	cloud.google.com/go/firestore v1.2.0 // indirect
	firebase.google.com/go v3.13.0+incompatible
	github.com/codeginga/locevt v0.0.0-20190214060509-b70a75df8e90
	github.com/go-chi/chi v4.0.2+incompatible
	github.com/go-chi/cors v1.0.0 // indirect
	github.com/go-playground/locales v0.13.0 // indirect
	github.com/go-playground/universal-translator v0.16.0 // indirect
	github.com/go-redis/redis v6.15.6+incompatible
	github.com/go-redis/redis/v7 v7.0.0-beta.4 // indirect
	github.com/golang/snappy v0.0.1 // indirect
	github.com/google/uuid v1.1.1
	github.com/gorilla/websocket v1.4.1
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/viper v1.4.0
	github.com/xdg/scram v0.0.0-20180814205039-7eeb5667e42c // indirect
	github.com/xdg/stringprep v1.0.0 // indirect
	go.mongodb.org/mongo-driver v1.1.2
	go.uber.org/dig v1.7.0
	google.golang.org/api v0.29.0
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.30.0
)
