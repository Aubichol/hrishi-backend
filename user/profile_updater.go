package user

import (
	"fmt"
	"time"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
)

//ProfileUpdater is an interface that defines the functionalities of a profile update
type ProfileUpdater interface {
	ProfileUpdate(userID string, data *dto.MeUpdate) (*dto.BaseResponse, error)
}

//profileUpdater holds profile updater
type profileUpdater struct {
	storeUsers storeuser.Users
}

//ProfileUpdate implements ProfileUpdater interface
func (p *profileUpdater) ProfileUpdate(userID string, data *dto.MeUpdate) (*dto.BaseResponse, error) {
	modelUsr, err := p.storeUsers.FindByID(userID)
	if err != nil {
		return nil, fmt.Errorf("%s:%w", err.Error(), &errors.Unknown{
			errors.Base{"could not found user", false},
		})
	}

	data.ToModel(modelUsr)
	modelUsr.UpdatedAt = time.Now().UTC()

	if err := p.storeUsers.Save(modelUsr); err != nil {
		return nil, fmt.Errorf("%s:%w", err.Error(), &errors.Unknown{
			errors.Base{"could not update profile user", false},
		})
	}

	return &dto.BaseResponse{
		Message: "profile updated",
		OK:      true,
	}, nil
}

//NewProfileUpdater provides ProfileUpdater
func NewProfileUpdater(storeUsers storeuser.Users) ProfileUpdater {
	return &profileUpdater{storeUsers}
}
