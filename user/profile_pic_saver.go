package user

import (
	"fmt"

	"gitlab.com/Aubichol/hrishi-backend/errors"
	storeuser "gitlab.com/Aubichol/hrishi-backend/store/user"
	"gitlab.com/Aubichol/hrishi-backend/user/dto"
)

//ProfilePicSaver is an interface that save a profile picture from the request
type ProfilePicSaver interface {
	SaveProfilePic(userID string, req dto.ProfilePicSaver) (*dto.BaseResponse, error)
}

//ProfilePicSaverFunc is a function type that implements the ProfilePicSaver interface
type ProfilePicSaverFunc func(userID string, req dto.ProfilePicSaver) (*dto.BaseResponse, error)

//SaveProfilePic implements the ProfilePicSaver interface
func (p ProfilePicSaverFunc) SaveProfilePic(userID string, req dto.ProfilePicSaver) (*dto.BaseResponse, error) {
	return p(userID, req)
}

//NewProfilePicSaver provides a ProfilePicSave
func NewProfilePicSaver(store storeuser.Users) ProfilePicSaver {
	if store == nil {
		panic("empty store")
	}

	f := func(userID string, req dto.ProfilePicSaver) (*dto.BaseResponse, error) {
		if err := store.SetProfilePic(userID, req.PictureID); err != nil {
			return nil, fmt.Errorf("%s:%w", err.Error(), &errors.Unknown{
				Base: errors.Base{
					Message: "Could not set profile picture", OK: false,
				},
			})
		}
		return nil, nil
	}

	return ProfilePicSaverFunc(f)
}
