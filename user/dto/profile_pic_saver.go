package dto

import (
	"encoding/json"
	"fmt"
	"io"

	"gitlab.com/Aubichol/hrishi-backend/errors"
)

//ProfilePicSaver stores profile picture id
type ProfilePicSaver struct {
	PictureID string `json:"picture_id"`
}

//FromReader decodes from request to json type data
func (p *ProfilePicSaver) FromReader(reader io.Reader) error {
	err := json.NewDecoder(reader).Decode(p)
	if err != nil {
		return fmt.Errorf("%s:%w", err.Error(), &errors.Invalid{
			Base: errors.Base{"invalid update data", false},
		})
	}

	return nil
}
