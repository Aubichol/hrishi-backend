package server

import (
	"gitlab.com/Aubichol/hrishi-backend/container"
	"gitlab.com/Aubichol/hrishi-backend/status"
)

//Status registers status related providers
func Status(c container.Container) {
	c.Register(status.NewCreate)
	c.Register(status.NewReader)
	c.Register(status.NewUpdate)
	c.Register(status.NewByUserReader)
	c.Register(status.NewNewsFeedReader)
}
