package server

import (
	"gitlab.com/Aubichol/hrishi-backend/chat"
	"gitlab.com/Aubichol/hrishi-backend/container"
)

//Chat registers chat related providers
func Chat(c container.Container) {
	c.Register(chat.NewCreateConversation)
	c.Register(chat.NewLatestReader)
	c.Register(chat.NewLatestUpdater)
	c.Register(chat.NewPreviousReader)
	c.Register(chat.NewPointerReader)
}
