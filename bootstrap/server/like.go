package server

import (
	"gitlab.com/Aubichol/hrishi-backend/container"
	"gitlab.com/Aubichol/hrishi-backend/like"
)

//Like registers like related providers
func Like(c container.Container) {
	c.Register(like.NewCreate)
	c.Register(like.NewRead)
	//c.Register(status.NewReader)
	c.Register(like.NewUpdate)
	c.Register(like.NewByStatusReader)
	c.Register(like.NewCountByStatusReader)
	c.Register(like.NewActivate)
}
