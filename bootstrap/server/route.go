package server

import (
	"gitlab.com/Aubichol/hrishi-backend/api/chat"
	"gitlab.com/Aubichol/hrishi-backend/api/comment"
	"gitlab.com/Aubichol/hrishi-backend/api/friendconnections"
	"gitlab.com/Aubichol/hrishi-backend/api/like"
	"gitlab.com/Aubichol/hrishi-backend/api/picture"
	"gitlab.com/Aubichol/hrishi-backend/api/profile"
	"gitlab.com/Aubichol/hrishi-backend/api/routeutils"
	"gitlab.com/Aubichol/hrishi-backend/api/status"
	"gitlab.com/Aubichol/hrishi-backend/api/user"
	"gitlab.com/Aubichol/hrishi-backend/api/wsroute"
	"gitlab.com/Aubichol/hrishi-backend/container"
)

//Route registers all the route providers to container
func Route(c container.Container) {
	c.RegisterGroup(user.RegistrationRoute, "route")
	c.RegisterGroup(user.LoginRoute, "route")
	c.RegisterGroup(user.SearchRoute, "route")
	c.RegisterGroup(friendconnections.OpenRequestRoute, "route")
	c.RegisterGroup(friendconnections.UpdateFriendRoute, "route")
	c.RegisterGroup(friendconnections.FriendListRoute, "route")
	c.RegisterGroup(routeutils.NewOptionRoute, "route")
	c.RegisterGroup(profile.UserRoute, "route")
	c.RegisterGroup(profile.OtherUserRoute, "route")
	c.RegisterGroup(chat.PostRoute, "route")
	c.RegisterGroup(chat.ReadRoute, "route")
	c.RegisterGroup(wsroute.WSRoute, "ws_route")
	c.RegisterGroup(profile.UpdateRoute, "route")
	c.RegisterGroup(picture.ProfilePicDownloadRoute, "route")
	c.RegisterGroup(picture.ProfilePicUploadRoute, "route")
	c.RegisterGroup(picture.UploadRoute, "route")
	c.RegisterGroup(picture.ListRoute, "route")
	c.RegisterGroup(picture.DownloadRoute, "route")
	c.RegisterGroup(picture.ProfilePicSetRoute, "route")
	c.RegisterGroup(user.ListRoute, "route")
	c.RegisterGroup(user.PublicListRoute, "route")
	c.RegisterGroup(chat.UpdateRoute, "route")
	c.RegisterGroup(chat.ReadPointerRoute, "route")
	c.RegisterGroup(status.CreateRoute, "route")
	c.RegisterGroup(status.ReadRoute, "route")
	c.RegisterGroup(status.UpdateRoute, "route")
	c.RegisterGroup(user.NewRegisterTokenRoute, "route")
	c.RegisterGroup(comment.CreateRoute, "route")
	c.RegisterGroup(comment.ReadRoute, "route")
	c.RegisterGroup(comment.UpdateRoute, "route")
	c.RegisterGroup(like.CreateRoute, "route")
	c.RegisterGroup(like.ReadRoute, "route")
	c.RegisterGroup(like.UpdateRoute, "route")
	c.RegisterGroup(status.ByUserRoute, "route")
	c.RegisterGroup(comment.ByStatusRoute, "route")
	c.RegisterGroup(like.ByStatusRoute, "route")
	c.RegisterGroup(like.CountByStatusRoute, "route")
	c.RegisterGroup(comment.CountByStatusRoute, "route")
	c.RegisterGroup(status.NewsFeedRoute, "route")
	c.RegisterGroup(like.ActivateRoute, "route")
}
