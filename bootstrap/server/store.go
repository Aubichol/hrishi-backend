package server

import (
	"gitlab.com/Aubichol/hrishi-backend/container"
	comment "gitlab.com/Aubichol/hrishi-backend/store/comment/mongo"
	conversation "gitlab.com/Aubichol/hrishi-backend/store/conversation/mongo"
	friendrequest "gitlab.com/Aubichol/hrishi-backend/store/friendrequest/mongo"
	like "gitlab.com/Aubichol/hrishi-backend/store/like/mongo"
	picture "gitlab.com/Aubichol/hrishi-backend/store/picture/mongo"
	status "gitlab.com/Aubichol/hrishi-backend/store/status/mongo"
	token "gitlab.com/Aubichol/hrishi-backend/store/token/mongo"
	user "gitlab.com/Aubichol/hrishi-backend/store/user/mongo"
)

//Store provides constructors for mongo db implementations
func Store(c container.Container) {
	c.Register(user.Store)
	c.Register(friendrequest.Store)
	c.Register(conversation.ReadPointersStore)
	c.Register(conversation.Store)
	c.Register(picture.Store)
	c.Register(status.Store)
	c.Register(token.Store)
	c.Register(comment.Store)
	c.Register(like.Store)
}
