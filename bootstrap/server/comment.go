package server

import (
	"gitlab.com/Aubichol/hrishi-backend/comment"
	"gitlab.com/Aubichol/hrishi-backend/container"
)

//Comment registers comment related providers
func Comment(c container.Container) {
	c.Register(comment.NewCreate)
	c.Register(comment.NewRead)
	c.Register(comment.NewUpdate)
	c.Register(comment.NewByStatusReader)
	c.Register(comment.NewCountByStatusReader)
}
