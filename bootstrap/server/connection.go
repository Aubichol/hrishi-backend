package server

import (
	"gitlab.com/Aubichol/hrishi-backend/connection"
	"gitlab.com/Aubichol/hrishi-backend/container"
)

//Connection registers all the connection related providers
func Connection(c container.Container) {
	c.Register(connection.NewAcceptor)
	c.Register(connection.NewRejector)
	c.Register(connection.NewBlocker)
	c.Register(connection.NewUnfriender)
	c.RegisterWithName(connection.NewPublicUserList, "public")
	c.Register(connection.NewUnblocker)
	c.RegisterWithName(connection.NewActiveFriendList, "active")
	c.RegisterWithName(connection.NewPendingFriendList, "pending")
	c.RegisterWithName(connection.NewUserList, "all")
	c.RegisterWithName(connection.NewBlockedFriendList, "blocked")
	c.Register(connection.NewRequester)
	c.Register(connection.NewChecker)
}
