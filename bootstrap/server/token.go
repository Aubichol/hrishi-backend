package server

import (
	"gitlab.com/Aubichol/hrishi-backend/container"
	"gitlab.com/Aubichol/hrishi-backend/token"
)

//Token registers token related providers
func Token(c container.Container) {
	c.Register(token.NewRegisterStore)
}
